<?php
/**
 * Created by PhpStorm.
 * User: vyadati
 * Date: 22-03-2021
 * Time: 09:39
 */

return [
    //'BASE_URL'=>"https://pekkish.glovision.co/pekkish-dev/api",
    //'BASE_URL'=>"http://localhost/pekkish-dev/api",
     // 'BASE_URL'=>"https://pekkish.glovision.co/pekkish/api",
    'BASE_URL'=>"http://localhost/pekkish/api",

    'ALL_COUPONS' => "/v1/cuisines-filter",
    'SIGNIN' => "/v1/auth/login",
    'SIGNUP' => "/shops/signup",
    'ORDERS' => "/v1/restaurant/order-details",
    'ORDERHISTORY' => "/v1/restaurant/order/history",
    'MENULIST' => "/v1/restaurant/menu",
    'CUISINES' => "/v1/cuisines-filter",
    'ADDITEM' => "/products/store",
    'SHOPS' => "/shops/list/",
    'ADDSHOP' => "/shops/insert/",

]

?>

@extends('layouts.app')
@section('content')

<div class="main-section">
    <div class="orders-notifications">
        <!-- <div class="bg-black">
          <div class="searching-for-orders max-content-width">
            Searching for new orders
          </div>
        </div> -->


        <!-- <div class="bg-green">
          <div class="received-orders max-content-width">
            <div class="received-orders-content">
              <img src="./images/usericon.svg" alt="">
              <div class="d-flex flex-wrap justify-content-center align-items-center">
                <div class="mx-3 new-order-text">You have new order!</div>

                <div class="mx-3 d-flex"><span>Total 4 Items </span> <span class="mx-1">|</span><span>$</span>
                  <span>2500.25</span>
                </div>
              </div>
            </div>
            <div class="d-flex align-items-center justify-content-center">
              <img class="mr-2" src="./images/alret.svg" alt="">
              <button class="see-details-btn">SEE THE DETAILS</button>
            </div>
          </div>
        </div> -->
    </div>

    <div class="max-content-width main-section-padding">
        <div class="d-flex align-items-center justify-content-between pb-2">
            <h4 class="heading">SALES INSIGHTS</h4>
            <form>
                <select name="days" id="days" class="select-drop-down">
                    <option value="7">last 7 days</option>

                </select>
            </form>
        </div>
        <div class="container-fluid p-0 sales">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-3">
                    <div class="sales-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="sale-type">New Sales</div>
                                <img class="mt-2" src="./images/netsales.svg" alt="">
                            </div>

                            <div class="text-to-end">
                                <div class="sale-percent-up"><span class="mx-2">23.29%</span> <img src="./images/uparrow-green.svg"
                                                                                                   alt=""></div>
                                <div class="sale-value"><span>$</span><span>10,999.32</span></div>
                                <div class="sale-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>
                    <div class="sales-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="sale-type">Total Orders</div>
                                <img class="mt-2" src="./images/totalorders.svg" alt="">
                            </div>

                            <div class="text-to-end">
                                <div class="sale-percent-down"><span class="mx-2">10.23%</span> <img src="./images/down-arrow-red.svg"
                                                                                                     alt=""></div>
                                <div class="sale-value"><span><span>$</span></span>2,345</div>
                                <div class="sale-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>
                    <div class="sales-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="sale-type">New Income</div>
                                <img class="mt-2" src="./images/netincome.svg" alt="">
                            </div>

                            <div class="text-to-end">
                                <div class="sale-percent-up"><span class="mx-2">23.29%</span> <img src="./images/uparrow-green.svg"
                                                                                                   alt=""></div>
                                <div class="sale-value"><span><span>$</span></span>10,999.32</div>
                                <div class="sale-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-9">

                    sale details chat
                </div>
            </div>

            <div class="row avg-time">
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="avg-time-card ">
                        <div class="avg-title">Avg. Preparation Time</div>
                        <div>
                            <div class="text-to-end">
                                <div class="avg-time-percent-up"><span class="mx-2">23.29%</span> <img
                                        src="./images/uparrow-green.svg" alt=""></div>
                                <div class="avg-time-value">00:15:25</div>
                                <div class="avg-time-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="avg-time-card">
                        <div class="avg-title">Avg. Waiting Time</div>
                        <div>
                            <div class="text-to-end">
                                <div class="avg-time-percent-down"><span class="mx-2">23.29%</span> <img
                                        src="./images/down-arrow-red.svg" alt=""></div>
                                <div class="avg-time-value">00:15:25</div>
                                <div class="avg-time-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="avg-time-card">
                        <div class="avg-title">Avg. Delivery Time</div>
                        <div>
                            <div class="text-to-end">
                                <div class="avg-time-percent-up"><span class="mx-2">23.29%</span> <img
                                        src="./images/uparrow-green.svg" alt=""></div>
                                <div class="avg-time-value">00:18:25</div>
                                <div class="avg-time-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 d-flex justify-content-end download mt-0">
                    <span class="text-color-green mx-2 f-14 f-medium ">Download Report</span> <img src="./images/download.svg"
                                                                                                   alt="">
                </div>

            </div>

            <div class="row banner-section">

                <div class="col-12">
                    <div class="position-relative ">
                        <img class="banner" src="./images/banner.png" alt="">
                        <div class="cta">
                            <button class="learn-more">learn more</button>
                            <button class="start-now">start now</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 mt-5 mb-2">
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <h4 class="heading">ITEMIZED INSIGHT REPORT</h4>
                        <form>
                            <select name="days" id="days" class="select-drop-down">
                                <option value="7">last 7 days</option>

                            </select>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row itemized-report">
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Popular Item (Most Selling)</div>
                        <div class="itemized-report-percent-up">
                            <span ><img src="./images/uparrow-green.svg" alt=""></span><span class="mx-1">23.29%</span>
                        </div>
                        <div class="itemized-report-value">Chicken Meals…</div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black"> chicken fry</span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Popular Category</div>
                        <div class="itemized-report-percent-down">
                            <span ><img src="./images/down-arrow-red.svg" alt=""></span><span class="mx-1">2.25%</span>
                        </div>
                        <div class="itemized-report-value">Combo Meals</div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black"> Combo Meals</span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Popular Offer</div>
                        <div class="itemized-report-percent-up">
                            <span ><img src="./images/uparrow-green.svg" alt=""></span><span class="mx-1">23.29%</span>
                        </div>
                        <div class="itemized-report-value text-uppercase">PEKPIC</div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black text-uppercase"> SALDAY</span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Sales Through Offers</div>
                        <div class="itemized-report-percent-up">
                            <span ><img src="./images/uparrow-green.svg" alt=""></span><span class="mx-1">2.25%</span>
                        </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>3900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>3900</span></span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">

                <div class="col-12">
                    <div class="d-flex justify-content-end download">
                        <span class="text-color-green mx-2 f-14 f-medium ">Download Report</span> <img src="./images/download.svg"
                                                                                                       alt="">
                    </div>
                </div>

            </div>


            <div class="line"></div>

            <div class="d-flex align-items-center justify-content-between pb-2">
                <h4 class="heading">POPULAR TIMINGS FOR BUSINESS</h4>
            </div>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-4">
                    <div class="timing-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="timing-type">Breakfast Time</div>
                                <img class="mt-2" src="./images/breakfast.png" alt="">
                            </div>

                            <div class="d-flex align-items-center" style="margin-top: -15px;">
                                <div class="from-time">
                                    <div class="f-20">6</div>
                                    <div class="am-pm">am</div>
                                </div>
                                <div class="mx-2">to</div>
                                <div class="to-time">
                                    <div class="f-20">8</div>
                                    <div class="am-pm">pm</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="timing-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="timing-type">Lunch Time</div>
                                <img class="mt-2" src="./images/lunch.png" alt="">
                            </div>

                            <div class="d-flex align-items-center" style="margin-top: -15px;">
                                <div class="from-time">
                                    <div class="f-20">11</div>
                                    <div class="am-pm">am</div>
                                </div>
                                <div class="mx-2">to</div>
                                <div class="to-time">
                                    <div class="f-20">3</div>
                                    <div class="am-pm">pm</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="timing-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="timing-type">Dinner Time</div>
                                <img class="mt-2" src="./images/dinner.png" alt="">
                            </div>

                            <div class="d-flex align-items-center" style="margin-top: -15px;">
                                <div class="from-time">
                                    <div class="f-20">6</div>
                                    <div class="am-pm">pm</div>
                                </div>
                                <div class="mx-2">to</div>
                                <div class="to-time">
                                    <div class="f-20">11</div>
                                    <div class="am-pm">pm</div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
                <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-8">

                    Day Wise Sales Details
                </div>
                <div class="col-12">
                    <div class="d-flex justify-content-end download">
                        <span class="text-color-green mx-2 f-14 f-medium ">Download Report</span> <img src="./images/download.svg"
                                                                                                       alt="">
                    </div>
                </div>
            </div>
            <div class="line"></div>
            <div class="d-flex align-items-center justify-content-between pb-2">
                <h4 class="heading">CUSTOMERS & RATINGS</h4>
            </div>
            <div class="row customer-rating">
                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                    <div class="customer-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="customer-type">New Customers</div>
                                <img class="mt-2" src="./images/newcustomer.svg" alt="">
                            </div>

                            <div class="text-to-end">
                                <div class="customer-percent-up"><span class="mx-2">23.29%</span>
                                    <img src="./images/uparrow-green.svg" alt="">
                                </div>
                                <div class="customer-value"><span>12856</span></div>
                                <div class="customer-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                    <div class="overall-rating">
                        <div class="row">
                            <div class="comment-margin col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <div class="overall-rating-star f-14">
                                    <p class="f-medium text-color-grey d-flex">Over all Rating</p>
                                    <div class="d-flex align-items-baseline">
                                        <span class="font-weight-bold f-24">4.7</span>
                                        <span class="mx-2">
                        <div>
                          <span class="fa fa-star star-checked"></span>
                          <span class="fa fa-star star-checked"></span>
                          <span class="fa fa-star star-checked"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                        </div>
                      </span>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-margin col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5">
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="f-10 text-color-grey rating-word">Excellent - <span>20</span></div>
                                    <div class="rating-percentage-bg">
                                        <div class="actual-rating-value excellent"></div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="f-10 text-color-grey rating-word">Good - <span>10</span></div>
                                    <div class="rating-percentage-bg">
                                        <div class="actual-rating-value good"></div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="f-10 text-color-grey rating-word">Average - <span>5</span></div>
                                    <div class="rating-percentage-bg">
                                        <div class="actual-rating-value average"></div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="f-10 text-color-grey rating-word">Below Avg - <span>2</span></div>
                                    <div class="rating-percentage-bg">
                                        <div class="actual-rating-value below-avg"></div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="f-10 text-color-grey rating-word">Poor - <span>1</span></div>
                                    <div class="rating-percentage-bg">
                                        <div class="actual-rating-value poor"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="comment-margin col-12 col-sm-12 col-md-3 col-lg-3 col-xl-4">
                                <div class="overall-rating-comment">
                                    <div class="f-12 d-flex">Its really good that you have used proper care while delivering my order
                                    </div>
                                    <div class="comment-gap d-flex align-items-center justify-content-between">
                                        <div>
                                            <span class="fa fa-star star-checked"></span>
                                            <span class="fa fa-star star-checked"></span>
                                            <span class="fa fa-star star-checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                        <span class="f-10 text-color-grey">21 JAN 21</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="d-flex justify-content-end download">
                        <span class="text-color-green mx-2 f-14 f-medium ">Download Report</span> <img src="./images/download.svg"
                                                                                                       alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

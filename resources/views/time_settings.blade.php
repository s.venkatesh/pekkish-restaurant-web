@extends('layouts.app')
@section('content')

<div class="main-section">
    <div class="max-content-width main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">DEFAULT FOOD PREPARATION TIME</span>
            </div>
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="time-setting-card">
                            <div class="d-flex f-14 f-medium res-title">
                                The Blue Mahoe Restaurant
                            </div>
                            <div class="mb-0 mb-3 f-14 text-color-grey ">Set default food preparation time</div>
                            <div class="d-flex justify-content-between">
                                <button class="timings-button f-12 f-medium">15 min</button>
                                <button class="timings-button f-12 f-medium">20 min</button>
                                <button class="timings-button-active f-12 f-medium">30 min</button>
                                <button class="timings-button f-12 f-medium">40 min</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="time-setting-card">
                            <div class="d-flex f-14 f-medium res-title">
                                Erica's Hideaway Restaurant </div>
                            <div class="mb-0 mb-3 f-14 text-color-grey ">Set default food preparation time</div>
                            <div class="d-flex justify-content-between">
                                <button class="timings-button-active f-12 f-medium">15 min</button>
                                <button class="timings-button f-12 f-medium">20 min</button>
                                <button class="timings-button f-12 f-medium">30 min</button>
                                <button class="timings-button f-12 f-medium">40 min</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="time-setting-card">
                            <div class="d-flex f-14 f-medium res-title">
                                Fireman's Lobster Pit </div>
                            <div class="mb-0 mb-3 f-14 text-color-grey ">Set default food preparation time</div>
                            <div class="d-flex justify-content-between">
                                <button class="timings-button-active f-12 f-medium">15 min</button>
                                <button class="timings-button f-12 f-medium">20 min</button>
                                <button class="timings-button f-12 f-medium">30 min</button>
                                <button class="timings-button f-12 f-medium">40 min</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="time-setting-card">
                            <div class="d-flex f-14 f-medium res-title">
                                Potwah Restaurant </div>
                            <div class="mb-0 mb-3 f-14 text-color-grey ">Set default food preparation time</div>
                            <div class="d-flex justify-content-between">
                                <button class="timings-button-active f-12 f-medium">15 min</button>
                                <button class="timings-button f-12 f-medium">20 min</button>
                                <button class="timings-button f-12 f-medium">30 min</button>
                                <button class="timings-button f-12 f-medium">40 min</button>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="time-setting-card">
                            <div class="d-flex f-14 f-medium res-title">
                                Murphy's West End </div>
                            <div class="mb-0 mb-3 f-14 text-color-grey ">Set default food preparation time</div>
                            <div class="d-flex justify-content-between">
                                <button class="timings-button-active f-12 f-medium">15 min</button>
                                <button class="timings-button f-12 f-medium">20 min</button>
                                <button class="timings-button f-12 f-medium">30 min</button>
                                <button class="timings-button f-12 f-medium">40 min</button>
                            </div>

                        </div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="time-setting-card">
                            <div class="d-flex f-14 f-medium res-title"> Zimbali's Mountain Cooking School</div>
                            <div class="mb-0 mb-3 f-14 text-color-grey ">Set default food preparation time</div>
                            <div class="d-flex justify-content-between">
                                <button class="timings-button f-12 f-medium">15 min</button>
                                <button class="timings-button f-12 f-medium">20 min</button>
                                <button class="timings-button-active f-12 f-medium">30 min</button>
                                <button class="timings-button f-12 f-medium">40 min</button>
                            </div>

                        </div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="time-setting-card">
                            <div class="d-flex f-14 f-medium res-title">Best In The West </div>
                            <div class="mb-0 mb-3 f-14 text-color-grey ">Set default food preparation time</div>
                            <div class="d-flex justify-content-between">
                                <button class="timings-button f-12 f-medium">15 min</button>
                                <button class="timings-button f-12 f-medium">20 min</button>
                                <button class="timings-button f-12 f-medium">30 min</button>
                                <button class="timings-button-active f-12 f-medium">40 min</button>
                            </div>

                        </div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="time-setting-card">
                            <div class="d-flex f-14 f-medium res-title"> The Lodge Restaurant at Tensing Pen Hotel </div>
                            <div class="mb-0 mb-3 f-14 text-color-grey ">Set default food preparation time</div>
                            <div class="d-flex justify-content-between">
                                <button class="timings-button f-12 f-medium">15 min</button>
                                <button class="timings-button f-12 f-medium">20 min</button>
                                <button class="timings-button-active f-12 f-medium">30 min</button>
                                <button class="timings-button f-12 f-medium">40 min</button>
                            </div>

                        </div>
                    </div>





                </div>

            </div>

        </div>

    </div>
</div>
@endsection
@section('model')
    <div>
        <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog" aria-labelledby="sidebar-option-lable">
            <div class="modal-dialog" role="sidebar options">
                <div class="modal-content bg-black">


                    <div class="modal-body sidebar-options">
                        <header>
                            <img class="logo" src="./images/pekkish-logo.svg" alt="">
                            <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button> -->
                        </header>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./dashboard.html">
                                        <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                        <span class=" menu-link">Dashboard</span></a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse"
                                       data-target="#orders-menu">
                                        <img class="nav-icons" src="./images/orders.svg" alt="">
                                        <span class="">Orders</span></a>
                                    <div class="collapse" id="orders-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="./orders.html"><span>Orders</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./order_history.html"><span>History</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./menu.html">
                                        <img class="nav-icons" src="./images/menu.svg" alt="">
                                        <span class="menu-link">Menu</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./bussiness_hours.html">
                                        <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                        <span
                                            class=" menu-link">Bussiness Hours</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./accounts.html">
                                        <img class="nav-icons" src="./images/accounts.svg" alt="">
                                        <span
                                            class=" menu-link">Accounts</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./feedback_reviews.html">
                                        <img class="nav-icons" src="./images/feeback.svg" alt="">
                                        <span class=" menu-link">Feedbacks
                                        & Reviews</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./offer_list.html">
                                        <img class="nav-icons" src="./images/offers.svg" alt="">
                                        <span class=" menu-link">Offers</span></a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate active-links" href="#settings-menu" data-toggle="collapse"
                                       data-target="#settings-menu">
                                        <img class="nav-icons" src="./images/settings.svg" alt="">
                                        <span class="">Settings</span></a>
                                    <div class="collapse" id="settings-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="./manage_employees.html"><span>Manage Employees</span></a>
                                                <a class="nav-link collapsed py-1 "
                                                   href="./manage_restaurant.html"><span>Manage Restaurants</span></a>
                                                <a class="nav-link collapsed py-1 active-links"
                                                   href="./time_settings.html"><span>Time Settings</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <div class="mt-5">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/logout.svg" alt="">
                                            <span class=" menu-link">Logout</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/help.svg" alt="">
                                            <span class=" menu-link">Help</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">

                                            <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                    </li>
                                </div>


                            </ul>

                        </nav>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog" aria-labelledby="user-profile">
            <div class="modal-dialog" role="profile dialog">
                <div class="modal-content bg-black">

                    <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="user-profile">Right Sidebar</h4>
                </div> -->

                    <div class="modal-body">

                        <div class="outerDivFull" >
                            <div class="switchToggle">
                                <input type="checkbox" id="switch">
                                <label for="switch">Toggle</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                            <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                            <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                        </div>
                        <div>
                            <nav class="navbar-options">
                                <ul class="nav flex-column flex-nowrap overflow-hidden">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Update Email Address</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Reset Password</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate " href="#">
                                            <span class=" menu-link">Update <Address></Address></span></a>
                                    </li>
                                </ul>
                        </div>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

    </div>
@endsection

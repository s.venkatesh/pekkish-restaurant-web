<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>Pekkish_end user/login</title>
</head>
<body class="login-bg">
<!-- login start -->
<div>
    <div class="form-wrapper">
        <img  class="form-img" src="images/pekkish-logo.png  " alt="">
        <form action="{{url('log-in')}}" class="form" method="post">
            @csrf
            <div class="input-group">
                <label for="username" class="label">Email Address</label>
                <input type="email" name="email" class="input" placeholder="Email Address">
                <span class="btn-show-top">
                    <img src="images/pwd_icon.svg" alt="" title="">
                  </span>
            </div>

            <div class="input-group">
                <div class="label-flex">
                    <label for="username" class="label">Password</label>
                </div>
                <input type="password" name="password" class="input" placeholder="Password">
                <span class="btn-show-pass">
                   <img src="images/email_icon.svg" alt="" title="">
                  </span>
                <a class="link mt-3 ml-auto" href="#" >Forgot Password?</a>
            </div>
            <div class="or-wrapper ">
                <span class="line"></span>
                <span class="or">Or</span>
                <span class="line"></span>
            </div>
            <div class="d-flex justify-content-center mb-4 pb-xl-3 pb-4 pt-3">
                <a href="#" class="mr-3"><img src="images/f.png"></a>
                <a href="#" class="mr-3"><img src="images/g.png"></a>
                <a href="#" class="mr-3"><img src="images/p.png"></a>

            </div>
            <!-- <a href="restaurant.html"><button class="login-btn">Log In</button></a> -->
            <div class="pt-3">
                <button class="btn login-btn" role="button">LOG IN</button>
            </div>

        </form>

        <div class="d-flex justify-content-center pt-5">
            <p class="login-p">Don't have an account?</p>
            <a href="create_account.html" class="link pl-2">SIGN UP NOW</a>
        </div>


    </div>

</div>
<!-- login end -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="d-flex justify-content-end login-terms">
                <a href="#" class="mr-3 terms-text">Privacy Policy</a>
                <a href="#" class="terms-text">Terms & Conditions</a>

            </div>

        </div>
    </div>
</div>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery-3.4.1.slim.min.js" ></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
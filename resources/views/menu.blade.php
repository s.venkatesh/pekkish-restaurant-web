@extends('layouts.app')
@section('content')
    <div class="main-section">
        <div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 bg-black pt-3 menu-list-fixed">
                        <nav class="navbar-options menu-list-items">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate active-links" href="#">
                                        <span class="menu-link ">Recommended</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Soups & Subs</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Starters</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Sandwiches</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Burgers</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Pizzas</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Pastas</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Breakfast</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Combo Meals</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Rice Bowls</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Greens & Healthy</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Desserts</span>
                                    </a>
                                    <a class="nav-link text-truncate " href="#">
                                        <span class="menu-link ">Wine</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>

                        <div class="menu-list-items d-flex justify-content-end">
                            <button class="add-section mt-3">
                                + Add Section
                            </button>
                        </div>

                    </div>
                    <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10 max-content-width main-section-padding">
                        <div class="res-cards d-flex align-items-center justify-content-between pb-2">
                            <h4 class="heading">Recommended</h4>
                            <button class="add-btn" data-toggle="modal" data-target="#addmenuid">+ Add Item</button>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-light">
                                <tr>
                                    <th>Item Name</th>
                                    <th>Available Days</th>
                                    <th>Menu For</th>
                                    <th>Price</th>
                                    <th>Rec. Pairings</th>
                                    <th>Availability</th>
                                    <th>Actions</th>
                                    <th>
                                        <div class="table-options-selection">
                                            <button type="button" class="btn btn-default btn-sm"
                                                    data-toggle="dropdown">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button>
                                            <ul class="dropdown-menu p-4">
                                                <li><a href="#" class="small" data-value="option1" tabIndex="-1">
                                                        <div>
                                                            <label class="check text-color-black-medium">Option 1
                                                                <input type="checkbox" name="is_name" checked>
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li><a href="#" class="small" data-value="option2" tabIndex="-1">
                                                        <div>
                                                            <label class="check">Option 2
                                                                <input type="checkbox" name="is_name">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li><a href="#" class="small" data-value="option3" tabIndex="-1">
                                                        <div>
                                                            <label class="check">Option 3
                                                                <input type="checkbox" name="is_name">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li><a href="#" class="small" data-value="option4" tabIndex="-1">
                                                        <div>
                                                            <label class="check">Option 4
                                                                <input type="checkbox" name="is_name">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li><a href="#" class="small" data-value="option5" tabIndex="-1">
                                                        <div>
                                                            <label class="check">Option 5
                                                                <input type="checkbox" name="is_name">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li><a href="#" class="small" data-value="option6" tabIndex="-1">
                                                        <div>
                                                            <label class="check">Option 6
                                                                <input type="checkbox" name="is_name">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li><a href="#" class="small" data-value="option7" tabIndex="-1">
                                                        <div>
                                                            <label class="check">Option 7
                                                                <input type="checkbox" name="is_name">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <p class="f-12 text-color-grey f-normal mb-0 mt-4">You can select
                                                    max 6</p>


                                            </ul>
                                        </div>

                                    </th>


                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Chicken Periperi</td>
                                    <td>All Days</td>
                                    <td>All</td>
                                    <td>
                                        <div class="d-flex">
                                            <span>$</span><span>220.00</span>
                                        </div>
                                    </td>
                                    <td>Chicken, Lorem…</td>
                                    <td>
                                        <div class="checkbox checbox-switch switch-primary">
                                            <label>
                                                <input type="checkbox" name="" checked="" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-around">
                                        <img src="./images/archive.svg" alt="" data-toggle="modal"
                                             data-target="#archieve">
                                        <img src="./images/edit.svg" alt="">
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Chicken Periperi</td>
                                    <td>Sun, Mon, Tue</td>
                                    <td>Breakfast</td>
                                    <td>
                                        <div class="d-flex">
                                            <span>$</span><span>220.00</span>
                                        </div>
                                    </td>
                                    <td>Chicken, Lorem…</td>
                                    <td>
                                        <div class="checkbox checbox-switch switch-primary">
                                            <label>
                                                <input type="checkbox" name="" checked="" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-around">
                                        <img src="./images/archive.svg" alt="">
                                        <img src="./images/edit.svg" alt="">
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Chicken Periperi</td>
                                    <td>Fri</td>
                                    <td>Lunch</td>
                                    <td>
                                        <div class="d-flex">
                                            <span>$</span><span>220.00</span>
                                        </div>
                                    </td>
                                    <td>Chicken, Lorem…</td>
                                    <td>
                                        <div class="checkbox checbox-switch switch-primary">
                                            <label>
                                                <input type="checkbox" name="" checked="" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-around">
                                        <img src="./images/archive.svg" alt="">
                                        <img src="./images/edit.svg" alt="">
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Chicken Periperi</td>
                                    <td>All Days</td>
                                    <td>All</td>
                                    <td>
                                        <div class="d-flex">
                                            <span>$</span><span>220.00</span>
                                        </div>
                                    </td>
                                    <td>Chicken, Lorem…</td>
                                    <td>
                                        <div class="checkbox checbox-switch switch-primary">
                                            <label>
                                                <input type="checkbox" name="" checked="" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-around">
                                        <img src="./images/archive.svg" alt="">
                                        <img src="./images/edit.svg" alt="">
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Chicken Periperi</td>
                                    <td>Mon</td>
                                    <td>Dinner</td>
                                    <td>
                                        <div class="d-flex">
                                            <span>$</span><span>220.00</span>
                                        </div>
                                    </td>
                                    <td>Chicken, Lorem…</td>
                                    <td>
                                        <div class="checkbox checbox-switch switch-primary">
                                            <label>
                                                <input type="checkbox" name="" checked="" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-around">
                                        <img src="./images/archive.svg" alt="">
                                        <img src="./images/edit.svg" alt="">
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Chicken Periperi</td>
                                    <td>All Days</td>
                                    <td>All</td>
                                    <td>
                                        <div class="d-flex">
                                            <span>$</span><span>220.00</span>
                                        </div>
                                    </td>
                                    <td>Chicken, Lorem…</td>
                                    <td>
                                        <div class="checkbox checbox-switch switch-primary">
                                            <label>
                                                <input type="checkbox" name="" checked="" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-around">
                                        <img src="./images/archive.svg" alt="">
                                        <img src="./images/edit.svg" alt="">
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Chicken Periperi</td>
                                    <td>All Days</td>
                                    <td>All</td>
                                    <td>
                                        <div class="d-flex">
                                            <span>$</span><span>220.00</span>
                                        </div>
                                    </td>
                                    <td>Chicken, Lorem…</td>
                                    <td>
                                        <div class="checkbox checbox-switch switch-primary">
                                            <label>
                                                <input type="checkbox" name="" checked="" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-around">
                                        <img src="./images/archive.svg" alt="">
                                        <img src="./images/edit.svg" alt="">
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Chicken Periperi</td>
                                    <td>All Days</td>
                                    <td>All</td>
                                    <td>
                                        <div class="d-flex">
                                            <span>$</span><span>220.00</span>
                                        </div>
                                    </td>
                                    <td>Chicken, Lorem…</td>
                                    <td>
                                        <div class="checkbox checbox-switch switch-primary">
                                            <label>
                                                <input type="checkbox" name="" checked="" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-around">
                                        <img src="./images/archive.svg" alt="">
                                        <img src="./images/edit.svg" alt="">
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Chicken Periperi</td>
                                    <td>All Days</td>
                                    <td>All</td>
                                    <td>
                                        <div class="d-flex">
                                            <span>$</span><span>220.00</span>
                                        </div>
                                    </td>
                                    <td>Chicken, Lorem…</td>
                                    <td>
                                        <div class="checkbox checbox-switch switch-primary">
                                            <label>
                                                <input type="checkbox" name="" checked="" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-around">
                                        <img src="./images/archive.svg" alt="">
                                        <img src="./images/edit.svg" alt="">
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Chicken Periperi</td>
                                    <td>All Days</td>
                                    <td>All</td>
                                    <td>
                                        <div class="d-flex">
                                            <span>$</span><span>220.00</span>
                                        </div>
                                    </td>
                                    <td>Chicken, Lorem…</td>
                                    <td>
                                        <div class="checkbox checbox-switch switch-primary">
                                            <label>
                                                <input type="checkbox" name="" checked="" />
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-around">
                                        <img src="./images/archive.svg" alt="">
                                        <img src="./images/edit.svg" alt="">
                                    </td>
                                    <td></td>
                                </tr>



                                </tbody>
                            </table>
                        </div>


                        <div class="d-flex justify-content-between download f-12 mt-0">
                            <div class="d-flex ">
                                <span>Rows per page:</span>
                                <span>
                                    <div class="form-group rows-per-page">
                                        <select class="form-control" id="selected-rows">
                                            <option>10</option>
                                            <option>20</option>
                                            <option>30</option>
                                            <option>40</option>
                                        </select>
                                    </div>
                                </span>
                                <span class="mx-4">1-10 of 10</span>
                                <span>
                                    <i class="fa fa-angle-left f-20 mx-3" aria-hidden="true"></i>
                                    <i class="fa fa-angle-right f-20" aria-hidden="true"></i>
                                </span>
                            </div>

                            <div>
                                <span class="text-color-green mx-2 f-14 f-medium ">Download Report</span>
                                <img src="./images/download.svg" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>





    <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog"
         aria-labelledby="sidebar-option-lable">
        <div class="modal-dialog" role="sidebar options">
            <div class="modal-content bg-black">


                <div class="modal-body sidebar-options">
                    <header>
                        <img class="logo" src="./images/pekkish-logo.svg" alt="">
                        <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                      aria-hidden="true">&times;</span></button> -->
                    </header>
                    <nav class="navbar-options">
                        <ul class="nav flex-column flex-nowrap overflow-hidden">
                            <li class="nav-item">
                                <a class="nav-link text-truncate active-links" href="{{url('dashboard')}}">
                                    <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                    <span class=" menu-link">Dashboard</span></a>
                            </li>

                            <li class="nav-item ">
                                <a class="nav-link collapsed text-truncate" href="#orders-menu"
                                   data-toggle="collapse"
                                   data-target="#orders-menu">
                                    <img class="nav-icons" src="./images/orders.svg" alt="">
                                    <span class="">Orders</span></a>
                                <div class="collapse" id="orders-menu" aria-expanded="false">
                                    <ul class="flex-column pl-2 nav">
                                        <li class="nav-item">
                                            <a class="nav-link collapsed py-1"
                                               href="{{url('orders')}}"><span>Orders</span></a>
                                            <a class="nav-link collapsed py-1"
                                               href="{{url('order_history')}}"><span>History</span></a>

                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="{{url('menu')}}">
                                    <img class="nav-icons" src="./images/menu.svg" alt="">
                                    <span class="menu-link">Menu</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="{{ url('business_hours/') }}">
                                    <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                    <span class=" menu-link">Bussiness Hours</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="{{ url('accounts/') }}">
                                    <img class="nav-icons" src="./images/accounts.svg" alt="">
                                    <span class=" menu-link">Accounts</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="{{ url('feedback_reviews/') }}">
                                    <img class="nav-icons" src="./images/feeback.svg" alt="">
                                    <span class=" menu-link">Feedbacks
                      & Reviews</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="{{ url('offer_list/') }}">
                                    <img class="nav-icons" src="./images/offers.svg" alt="">
                                    <span class=" menu-link">Offers</span></a>
                            </li>


                            <li class="nav-item ">
                                <a class="nav-link collapsed text-truncate" href="#settings-menu"
                                   data-toggle="collapse"
                                   data-target="#settings-menu">
                                    <img class="nav-icons" src="./images/settings.svg" alt="">
                                    <span class="">Settings</span></a>
                                <div class="collapse" id="settings-menu" aria-expanded="false">
                                    <ul class="flex-column pl-2 nav">
                                        <li class="nav-item">
                                            <a class="nav-link collapsed py-1" href="{{ url('manage_employees/')}}"><span>Manage
                            Employees</span></a>
                                            <a class="nav-link collapsed py-1"
                                               href="{{ url('manage_restaurant/')}}"><span>Manage
                            Restaurants</span></a>
                                            <a class="nav-link collapsed py-1" href="{{ url('time_settings/')}}"><span>Time Settings</span></a>

                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <div class="mt-5">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <img class="nav-icons" src="./images/logout.svg" alt="">
                                        <span class=" menu-link">Logout</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <img class="nav-icons" src="./images/help.svg" alt="">
                                        <span class=" menu-link">Help</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">

                                        <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                </li>
                            </div>


                            <!-- <li class="nav-item">
                              <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse" data-target="#orders-menu"><i class="fa fa-table"></i> <span class="">Reports</span></a>
                              <div class="collapse" id="orders-menu" aria-expanded="false">
                                  <ul class="flex-column pl-2 nav">
                                      <li class="nav-item"><a class="nav-link py-0" href="#"><span>Orders</span></a></li>
                                      <li class="nav-item">
                                          <a class="nav-link collapsed py-1" href="#orders-menusub1" data-toggle="collapse" data-target="#orders-menusub1"><span>Customers</span></a>
                                          <div class="collapse" id="orders-menusub1" aria-expanded="false">
                                              <ul class="flex-column nav pl-4">
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-clock-o"></i> Daily </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-dashboard"></i> Dashboard </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-bar-chart"></i> Charts </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-compass"></i> Areas </a>
                                                  </li>
                                              </ul>
                                          </div>
                                      </li>
                                  </ul>
                              </div>
                          </li> -->
                        </ul>

                    </nav>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->


    <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog"
         aria-labelledby="user-profile">
        <div class="modal-dialog" role="profile dialog">
            <div class="modal-content bg-black">

                <!-- <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="user-profile">Right Sidebar</h4>
            </div> -->

                <div class="modal-body">

                    <div class="outerDivFull">
                        <div class="switchToggle">
                            <input type="checkbox" id="switch">
                            <label for="switch">Toggle</label>
                        </div>
                    </div>
                    <div class="text-center">
                        <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                        <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                        <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                    </div>
                    <div>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <span class=" menu-link">Update Email Address</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <span class=" menu-link">Reset Password</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate " href="#">
                                        <span class=" menu-link">Update <Address></Address></span></a>
                                </li>
                            </ul>
                    </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <form id="manage-menu-form" method="POST" action="{{url('addItem')}}">
        @csrf
        <div class="modal add-menu right fade " id="addmenuid" tabindex="-1" role="dialog" aria-labelledby="add-menu">
            <div class="modal-dialog" id="addnewmenu" role="add menu">
                <div class="modal-content model-row-colum ">
                    <div class="modal-body pb-4 dialog-options-responsive">
                        <div class="bg-black text-color-white negative-margin" style="display:none;" id="showSearchDiv">
                            <nav class="navbar-options menu-list-items d-flex ">
                                <ul class="nav flex-column flex-nowrap ">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate active-links" href="#">
                                            <span class="menu-link ">Recommended</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Soups & Subs</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Starters</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Sandwiches</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Burgers</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Pizzas</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Pastas</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Breakfast</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Combo Meals</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Rice Bowls</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Greens & Healthy</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Desserts</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Wine</span>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate active-links" href="#">
                                            <div>
                                                <label class="check">Item 1
                                                    <input type="checkbox" name="is_name">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </a>
                                        <a class="nav-link text-truncate active-links" href="#">
                                            <div>
                                                <label class="check">Item 2
                                                    <input type="checkbox" name="is_name">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </a>
                                        <a class="nav-link text-truncate active-links" href="#">
                                            <div>
                                                <label class="check">Item 3
                                                    <input type="checkbox" name="is_name">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </a>
                                        <a class="nav-link text-truncate active-links" href="#">
                                            <div>
                                                <label class="check">Item 4
                                                    <input type="checkbox" name="is_name">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </a>

                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="overflow-dialog">
                            <div class="f-20 f-medium">Add Item</div>
                            <div class="form-group input-material">
                                <input type="text" class="form-control" name="name" id="itemSearch" required>
                                <label for="itemSearch">Item Name</label>
                            </div>

                            <div class="form-group input-material">
                                <textarea class="form-control" name="description" id="textarea-field" rows="2" required></textarea>
                                <label for="textarea-field">Item Description</label>
                            </div>

                            <div class="d-flex justify-content-end f-10 text-color-grey" style="margin-top: -20px;">
                                0 / 40</div>
                            <div class="upload-image-section">
                                <img src="" alt="" id="image-preview" class="img-fluid" height="height:100px;">
                                <label class="btn btn-default btn-sm center-block btn-file">
                                    <span class="text-color-grey">+ ADD IMAGE (500px X 600px)</span>
                                    <input name="photos" type="file" style="display: none;" onchange="readURL(this);">
                                </label>
                            </div>
                            <div class="row">
                                <div class="container-fluid" style="margin: 0 0 -28px 0;">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" name="unit_price" class="form-control" id="ItemPrice" required>
                                                <label for="ItemPrice">Item Price</label>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" name="service_line" class="form-control" id="Select Service Line"
                                                       required>
                                                <label for="Select Service Line">Select Service Line</label>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select name="available_days[]" multiple id="inputState" class="form-control">
                                                    <option value="0">Sun</option>
                                                    <option value="1">Mon</option>
                                                    <option value="2">Tue</option>
                                                    <option value="3">Wed</option>
                                                    <option value="4">Thu</option>
                                                    <option value="5">Fri</option>
                                                    <option value="6">Sat</option>
                                                </select>
                                                <label for="Available Days">Available Days</label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select name="menu_items_desc" id="inputState" class="form-control">
                                                    <option>Choose...</option>
                                                    <option>Option 1</option>
                                                    <option>Option 2</option>
                                                </select>
                                                <label for="Menu For">Menu For</label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 mt-1">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" name="set_max_order_number" class="form-control" id="Set Max Order Number"
                                                       required>
                                                <label for="Set Max Order Number">Set Max Order Number</label>
                                            </div>
                                        </div>


                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select id="inputState" name="cuisine_id" class="form-control">
                                                    <option>Choose...</option>
                                                    {{--<option>Option 1</option>
                                                    <option>Option 2</option>--}}
                                                @foreach ($cuisines as $cuisine)
                                                    <option value="{{$cuisine['id']}}">{{$cuisine['name']}}</option>
                                                @endforeach
                                                </select>
                                                @if ($errors->has('cuisines'))
                                                    <div class="text-danger">{{ $errors->first('cuisines') }}</div>
                                                @endif
                                                <label for="Cusine Type">Cusine Type</label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="mt-5 mb-3">
                                <p class="mb-2 text-color-grey f-14">Availability Status</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Available
                                                <input type="radio" name="available_ststus" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Not Available
                                                <input type="radio" name="available_ststus">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <label class="check">Modifier Required
                                    <input type="checkbox" name="Modifier">
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <div class="modifiactions-required">
                                <div class="d-flex align-items-end justify-content-end mb-0">
                                    <label class="radio mx-3">Optional
                                        <input type="radio" name="optional">
                                        <span class="checkround"></span>
                                    </label>
                                    <label class="radio">Required
                                        <input type="radio" name="optional">
                                        <span class="checkround"></span>
                                    </label>
                                </div>
                                <div>
                                    <div class="form-group input-material mb-5">
                                        <input type="text" class="form-control" id="ModifierName" required>
                                        <label for="Modifier Name">Modifier Name</label>
                                    </div>
                                    <div class="my-5">
                                        <p class="mb-3 text-color-grey f-14">Options for Selection</p>
                                        <div class="container-fluid p-0">
                                            <div class="row">
                                                <div class="col-6">
                                                    <label class="radio">Select at least one
                                                        <input type="radio" name="selection"
                                                               checked>
                                                        <span class="checkround"></span>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <label class="radio">Multiple Selection Required
                                                        <input type="radio" name="selection">
                                                        <span class="checkround"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="mt-2 mb-2">
                                        <p class="mb-0 text-color-grey f-14">Options</p>
                                        <div class="container-fluid p-0">
                                            <div class="row" id="addvariants">
                                                <div class="col-8">
                                                    <div class="form-group input-material mr-2">
                                                        <input type="text" name="variants[1]['variant']" class="form-control" id="opt1" required>
                                                        <label for="opt1">Name the Option - 1</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group input-material mr-2">
                                                        <input type="number" name="variants[1]['price']" step="0.01" class="form-control" id="Price" required>
                                                        <label for="Price">Price</label>
                                                    </div>
                                                </div>
                                                <div class="col-8">
                                                    <div class="form-group input-material mr-2">
                                                        <input type="text" name="variants[2]['variant']" class="form-control" id="opt2" required>
                                                        <label for="opt2">Name the Option - 2</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group input-material mr-2">
                                                        <input type="number" name="variants[2]['price']" step="0.01" class="form-control" id="Price2"
                                                               required>
                                                        <label for="Price2">Price</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="addmore" class="add_more d-flex align-items-center justify-content-center text-color-grey">
                                + ADD MORE
                            </div>

                            <div>
                                <div class="mb-3">
                                    <label class="check">Recommended Pairing
                                        <input type="checkbox" name="is_name">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="flex">
                                    <button class="employee-options option-selected">
                                        <span>Chicken Periperi</span><span class="ml-2"><img
                                                src="./images/clearoption.svg" alt=""></span></button>
                                    <button class="employee-options option-selected">
                                        <span>Chicken Periperi</span><span class="ml-2"><img
                                                src="./images/clearoption.svg" alt=""></span></button>

                                </div>

                            </div>

                            <div class="mt-5 d-flex align-items-center justify-content-between">

                                <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                                <button class="addbtn">ADD ITEM</button>
                            </div>
                        </div>




                    </div>

                </div><!-- modal-content -->

            </div><!-- modal-dialog -->
        </div><!-- modal -->
    </form>

    <!-- Modal -->
    <div class="modal fade" id="archieve" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body p-4">
                    <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                        <img src="./images/dialogclose.svg" alt="">
                    </div>
                    <p class="f-20 f-medium">Are you sure!</p>
                    <p class="f-12 mb-0">You want to archieve the item “Chicken Periperi"</p>
                </div>
                <div class="mb-2">
                    <div class=" mb-3 d-flex align-items-center justify-content-around">

                        <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                aria-label="Close">NO</button>
                        <button class="addbtn w-100 mx-3 f-medium" data-dismiss="modal">YES</button>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        var options = [];

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                val = $target.attr('data-value'),
                $inp = $target.find('input'),
                idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () { $inp.prop('checked', false) }, 0);
            } else {
                options.push(val);
                setTimeout(function () { $inp.prop('checked', true) }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });
    </script>

    <script>
        $.fn.materializeInputs = function (selectors) {

            // default param with backwards compatibility
            if (typeof (selectors) === 'undefined') selectors = "input, textarea";

            // attribute function
            function setInputValueAttr(element) {
                element.setAttribute('value', element.value);
            }

            // set value attribute at load
            this.find(selectors).each(function () {
                setInputValueAttr(this);
            });

            // on keyup
            this.on("keyup", selectors, function () {
                setInputValueAttr(this);
            });
        };

        /**
         * Material Inputs
         */
        $('body').materializeInputs();


    </script>

    <script>
        function checkValue(element) {
            // check if the input has any value (if we've typed into it)
            if ($(element).val())
                $(element).addClass('has-value');
            else
                $(element).removeClass('has-value');
        }

        $(document).ready(function () {
            // Run on page load
            $('.form-control').each(function () {
                checkValue(this);
            })
            // Run on input exit
            $('.form-control').blur(function () {
                checkValue(this);
            });

        });
    </script>

    <script>
        $(document).ready(function() {
            $("#itemSearch").keyup(function() {
                var x = document.getElementById('showSearchDiv');
                if($(this).val() == "") {
                    x.style.display = 'none';
                } else {
                    x.style.display = 'block';
                }
            });
        });
    </script>
    <script>
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image-preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#profile").change(function () {
            readURL(this);
        });



        $(document).ready(function () {
            var i = 2;
            $('#addmore').on('click', function () {
                i++;
                $('#addvariants').append('<div id="i" class="col-8">' +
                    '<div class="form-group input-material mr-2">' +
                '<input type="text" name="variants[i][variant]" class="form-control" id="opt2" required>' +
                    '<label for="opt2">Name the Option - '+i+'</label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-4">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="number" name="variants[i][price]" step="0.01" class="form-control" id="Price2" required>' +
                    '<label for="Price2">Price</label>' +
                    '</div>' +
                    '</div>');
            });
        });



    </script>
@endsection

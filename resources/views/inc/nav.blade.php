<div class="shadow-pk sticky-top">
    <div class="main-header max-content-width">
        <div class="d-flex align-items-center justify-content-center">
            <img class="menuicon" src="{{url('/')}}/images/menuicon.svg" alt="" data-toggle="modal" data-target="#sidebaroptionid">
            <img src="./images/pekkish-logo.svg" alt="">
        </div>
        <div class="d-flex align-items-center justify-content-center">
        <span class="usericons">
          <img src="./images/defalutusericon.svg" alt="">
          <span class="dot bg-red"></span>
        </span>
            <div class="user-details">
                <div class="mx-2" data-toggle="modal" data-target="#userprofileid">
                    <p class="user-name">Hello, Omari</p>
                    <div class="role">Account Manager</div>
                </div>
                <img src="./images/downarrowicon.svg" alt="">
            </div>

        </div>
    </div>
</div>
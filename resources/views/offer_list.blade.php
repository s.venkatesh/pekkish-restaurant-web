@extends('layouts.app')
@section('content')


<div class="main-section">
    <div class="max-content-width main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">OFFERS LIST</span>
                <button class="add-btn" data-toggle="modal" data-target="#addmenuid">+ Add New Offer</button>

            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 ">
                        <div class="offer-cards">
                            <div class="d-flex align-items-center justify-content-between mb-5">
                                <span class="status-view f-14 f-medium">ACTIVE</span>
                                <span>
                                        <img class="mx-2" src="./images/card-edit.svg" alt="">
                                        <img src="./images/delete.svg" alt="">
                                    </span>
                            </div>
                            <div class="text-center f-16 f-bold">SALARYDAY</div>
                            <div class="mb-0 pt-1 mb-4 f-14 text-color-grey text-center">
                                <div>New year promotion</div>
                                <div>Save 10% OFF on wines</div>
                            </div>
                            <div class="text-center f-14 f-medium">100 of 250 Coupon Users</div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 ">
                        <div class="offer-cards">
                            <div class="d-flex align-items-center justify-content-between mb-5">
                                <span class="status-view f-14 f-medium">ACTIVE</span>
                                <span>
                                        <img class="mx-2" src="./images/card-edit.svg" alt="">
                                        <img src="./images/delete.svg" alt="">
                                    </span>
                            </div>
                            <div class="text-center f-16 f-bold">PARTYMODE</div>
                            <div class="mb-0 pt-1 mb-4 f-14 text-color-grey text-center">
                                <div>New year promotion</div>
                                <div>Save 50% OFF on wines</div>
                            </div>
                            <div class="text-center f-14 f-medium">150 of 250 Coupon Users</div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 ">
                        <div class="offer-cards">
                            <div class="d-flex align-items-center justify-content-between mb-5">
                                <span class="status-view-expired f-14 f-medium ">EXPIRED </span>
                                <span>
                                        <img class="mx-2" src="./images/card-edit.svg" alt="">
                                        <img src="./images/delete.svg" alt="">
                                    </span>
                            </div>
                            <div class="text-center f-16 f-bold">WEEKEND</div>
                            <div class="mb-0 pt-1 mb-4 f-14 text-color-grey text-center">
                                <div>New year promotion</div>
                                <div>Save 15% OFF on wines</div>
                            </div>
                            <div class="text-center f-14 f-medium">150 of 250 Coupon Users</div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 ">
                        <div class="offer-cards">
                            <div class="d-flex align-items-center justify-content-between mb-5">
                                <span class="status-view f-14 f-medium">ACTIVE</span>
                                <span>
                                        <img class="mx-2" src="./images/card-edit.svg" alt="">
                                        <img src="./images/delete.svg" alt="">
                                    </span>
                            </div>
                            <div class="text-center f-16 f-bold">SALARYDAY</div>
                            <div class="mb-0 pt-1 mb-4 f-14 text-color-grey text-center">
                                <div>New year promotion</div>
                                <div>Save 10% OFF on wines</div>
                            </div>
                            <div class="text-center f-14 f-medium">100 of 250 Coupon Users</div>
                        </div>
                    </div>




                </div>

            </div>

        </div>

    </div>
</div>
@endsection

@section('model')
    <div>
        <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog"
             aria-labelledby="sidebar-option-lable">
            <div class="modal-dialog" role="sidebar options">
                <div class="modal-content bg-black">


                    <div class="modal-body sidebar-options">
                        <header>
                            <img class="logo" src="./images/pekkish-logo.svg" alt="">
                            <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                          aria-hidden="true">&times;</span></button> -->
                        </header>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate active-links" href="{{url('dashboard')}}">
                                        <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                        <span class=" menu-link">Dashboard</span></a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#orders-menu"
                                       data-toggle="collapse"
                                       data-target="#orders-menu">
                                        <img class="nav-icons" src="./images/orders.svg" alt="">
                                        <span class="">Orders</span></a>
                                    <div class="collapse" id="orders-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="{{url('orders')}}"><span>Orders</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="{{url('order_history')}}"><span>History</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{url('menu')}}">
                                        <img class="nav-icons" src="./images/menu.svg" alt="">
                                        <span class="menu-link">Menu</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('business_hours/') }}">
                                        <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                        <span class=" menu-link">Bussiness Hours</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('accounts/') }}">
                                        <img class="nav-icons" src="./images/accounts.svg" alt="">
                                        <span class=" menu-link">Accounts</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('feedback_reviews/') }}">
                                        <img class="nav-icons" src="./images/feeback.svg" alt="">
                                        <span class=" menu-link">Feedbacks
                      & Reviews</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('offer_list/') }}">
                                        <img class="nav-icons" src="./images/offers.svg" alt="">
                                        <span class=" menu-link">Offers</span></a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#settings-menu"
                                       data-toggle="collapse"
                                       data-target="#settings-menu">
                                        <img class="nav-icons" src="./images/settings.svg" alt="">
                                        <span class="">Settings</span></a>
                                    <div class="collapse" id="settings-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1" href="{{ url('manage_employees/')}}"><span>Manage
                            Employees</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="{{ url('manage_restaurant/')}}"><span>Manage
                            Restaurants</span></a>
                                                <a class="nav-link collapsed py-1" href="{{ url('time_settings/')}}"><span>Time Settings</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <div class="mt-5">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/logout.svg" alt="">
                                            <span class=" menu-link">Logout</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/help.svg" alt="">
                                            <span class=" menu-link">Help</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">

                                            <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                    </li>
                                </div>


                                <!-- <li class="nav-item">
                                  <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse" data-target="#orders-menu"><i class="fa fa-table"></i> <span class="">Reports</span></a>
                                  <div class="collapse" id="orders-menu" aria-expanded="false">
                                      <ul class="flex-column pl-2 nav">
                                          <li class="nav-item"><a class="nav-link py-0" href="#"><span>Orders</span></a></li>
                                          <li class="nav-item">
                                              <a class="nav-link collapsed py-1" href="#orders-menusub1" data-toggle="collapse" data-target="#orders-menusub1"><span>Customers</span></a>
                                              <div class="collapse" id="orders-menusub1" aria-expanded="false">
                                                  <ul class="flex-column nav pl-4">
                                                      <li class="nav-item">
                                                          <a class="nav-link p-1" href="#">
                                                              <i class="fa fa-fw fa-clock-o"></i> Daily </a>
                                                      </li>
                                                      <li class="nav-item">
                                                          <a class="nav-link p-1" href="#">
                                                              <i class="fa fa-fw fa-dashboard"></i> Dashboard </a>
                                                      </li>
                                                      <li class="nav-item">
                                                          <a class="nav-link p-1" href="#">
                                                              <i class="fa fa-fw fa-bar-chart"></i> Charts </a>
                                                      </li>
                                                      <li class="nav-item">
                                                          <a class="nav-link p-1" href="#">
                                                              <i class="fa fa-fw fa-compass"></i> Areas </a>
                                                      </li>
                                                  </ul>
                                              </div>
                                          </li>
                                      </ul>
                                  </div>
                              </li> -->
                            </ul>

                        </nav>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog"
             aria-labelledby="user-profile">
            <div class="modal-dialog" role="profile dialog">
                <div class="modal-content bg-black">

                    <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="user-profile">Right Sidebar</h4>
                </div> -->

                    <div class="modal-body">

                        <div class="outerDivFull">
                            <div class="switchToggle">
                                <input type="checkbox" id="switch">
                                <label for="switch">Toggle</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                            <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                            <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                        </div>
                        <div>
                            <nav class="navbar-options">
                                <ul class="nav flex-column flex-nowrap overflow-hidden">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Update Email Address</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Reset Password</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate " href="#">
                                            <span class=" menu-link">Update <Address></Address></span></a>
                                    </li>
                                </ul>
                        </div>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

        <form id="manage-menu-form">
            <div class="modal add-menu right fade " id="addmenuid" tabindex="-1" role="dialog"
                 aria-labelledby="add-menu">
                <div class="modal-dialog" id="addnewmenu" role="add menu">
                    <div class="modal-content model-row-colum ">
                        <div class="modal-body pb-4 dialog-options-responsive">
                            <div class="bg-black text-color-white negative-margin" style="display:none;"
                                 id="showSearchDiv">
                                <nav class="navbar-options menu-list-items d-flex ">
                                    <ul class="nav flex-column flex-nowrap ">
                                        <li class="nav-item">
                                            <a class="nav-link text-truncate active-links" href="#">
                                                <span class="menu-link ">Recommended</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Soups & Subs</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Starters</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Sandwiches</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Burgers</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Pizzas</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Pastas</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Breakfast</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Combo Meals</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Rice Bowls</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Greens & Healthy</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Desserts</span>
                                            </a>
                                            <a class="nav-link text-truncate " href="#">
                                                <span class="menu-link ">Wine</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                        <li class="nav-item">
                                            <a class="nav-link text-truncate active-links" href="#">
                                                <div>
                                                    <label class="check">Item 1
                                                        <input type="checkbox" name="is_name">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </a>
                                            <a class="nav-link text-truncate active-links" href="#">
                                                <div>
                                                    <label class="check">Item 2
                                                        <input type="checkbox" name="is_name">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </a>
                                            <a class="nav-link text-truncate active-links" href="#">
                                                <div>
                                                    <label class="check">Item 3
                                                        <input type="checkbox" name="is_name">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </a>
                                            <a class="nav-link text-truncate active-links" href="#">
                                                <div>
                                                    <label class="check">Item 4
                                                        <input type="checkbox" name="is_name">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </a>

                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="overflow-dialog">
                                <div class="f-20 f-medium">Add New Offer</div>
                                <!-- <div class="form-group input-material">
                                    <input type="text" class="form-control" id="itemSearch" required>
                                    <label for="itemSearch">Item Name</label>
                                </div> -->

                                <div class="form-group input-material">
                                    <textarea class="form-control" id="Promocode" rows="1" required></textarea>
                                    <label for="Promocode">Promocode</label>
                                </div>

                                <div class="d-flex justify-content-end f-10 text-color-grey" style="margin-top: -20px;">
                                    0 / 6</div>
                                <div class="mt-2 mb-0">
                                    <p class="mb-0 text-color-grey f-14">Offer Start & End Dates</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="text" class="form-control" id="StartDate" required
                                                           onfocus="(this.type='date')" onblur="(this.type='text')">
                                                    <label for="StartDate" class="datetype">Start Date</label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="text" class="form-control" id="ExpiryDate" required
                                                           onfocus="(this.type='date')" onblur="(this.type='text')">
                                                    <label for="ExpiryDate" class="datetype">Expiry Date</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-2 mb-0">
                                    <p class="mb-2 text-color-grey f-14">Offer Value</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <label class="radio">Dollar
                                                    <input type="radio" name="Dollar" checked>
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label class="radio">Percentage
                                                    <input type="radio" name="Dollar" checked>
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="flex">
                                    <button class="employee-options option-selected">
                                        <span>5%</span><span class="ml-2"></span>
                                    </button>

                                    <button class="employee-options ">
                                        <span>10%</span><span class="ml-2"></span>
                                    </button>

                                    <button class="employee-options ">
                                        <span>15%</span><span class="ml-2"></span>
                                    </button>
                                    <!--
                                    <button class="employee-options ">
                                        <span>20%</span><span class="ml-2"></span>
                                    </button>

                                    <button class="employee-options ">
                                        <span>Buy 1 Get 1 (50%)%</span><span class="ml-2"></span>
                                    </button> -->


                                </div>

                                <div>
                                    <p class="mb-2 mt-4 text-color-grey f-14">Days to Run Offer</p>
                                    <div class="d-flex align-items-center flex-wrap">
                                        <button class="working-hours-selected">S</button>
                                        <button class="working-hours-selection">M</button>
                                        <button class="working-hours-selection">T</button>
                                        <button class="working-hours-selection">W</button>
                                        <button class="working-hours-selection">T</button>
                                        <button class="working-hours-selection">F</button>
                                        <button class="working-hours-selection">S</button>

                                    </div>
                                </div>

                                <div class="mt-4 mb-0">
                                    <p class="mb-2 mt-2 text-color-grey f-14">Specify Items for this Offer</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="radio">Entire Menu
                                                    <input type="radio" name="menu" checked>
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                            <div class="col-4">
                                                <label class="radio">Specific Category
                                                    <input type="radio" name="menu" >
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>

                                            <div class="col-4">
                                                <label class="radio">Specific Item
                                                    <input type="radio" name="menu" >
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="mt-3 mb-0">
                                    <p class="mb-2 text-color-grey f-14">Offer Value</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <label class="radio">Dollar
                                                    <input type="radio" name="Dollar" checked>
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label class="radio">Percentage
                                                    <input type="radio" name="Dollar" checked>
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="flex">
                                    <button class="employee-options option-selected">
                                        <span>$200</span><span class="ml-2"></span>
                                    </button>



                                </div>

                                <div class="mt-4 mb-0">
                                    <p class="mb-2 text-color-grey f-14">Set Minimum Usage</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <label class="radio">Per Person
                                                    <input type="radio" name="Usage" checked>
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label class="radio">Unlimited
                                                    <input type="radio" name="Usage" >
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="mt-2 mb-0">
                                    <p class="mb-0 text-color-grey f-14">Set Timings</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="time" class="form-control" id="Start" required
                                                    >
                                                    <label for="Start" class="datetype">Start Date</label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="time" class="form-control" id="end" required
                                                    >
                                                    <label for="end" class="datetype">End Date</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="mt-2 mb-0">
                                    <p class="mb-1 text-color-grey f-14">Set Maximum and Minimum Purchase Limit</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="text" class="form-control" id="min" required
                                                    >
                                                    <label for="min" class="datetype">Minimum Limit</label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="text" class="form-control" id="max" required
                                                    >
                                                    <label for="max" class="datetype">Maximum Limit</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                                <div class="mt-5 d-flex align-items-center justify-content-between">

                                    <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                                    <button class="addbtn" data-dismiss="modal">ADD OFFER</button>
                                </div>
                            </div>




                        </div>

                    </div><!-- modal-content -->

                </div><!-- modal-dialog -->
            </div><!-- modal -->
        </form>

    </div>
@endsection

@section('script')

    <script>
        var options = [];

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                val = $target.attr('data-value'),
                $inp = $target.find('input'),
                idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () { $inp.prop('checked', false) }, 0);
            } else {
                options.push(val);
                setTimeout(function () { $inp.prop('checked', true) }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });
    </script>

    <script>
        $.fn.materializeInputs = function (selectors) {

            // default param with backwards compatibility
            if (typeof (selectors) === 'undefined') selectors = "input, textarea";

            // attribute function
            function setInputValueAttr(element) {
                element.setAttribute('value', element.value);
            }

            // set value attribute at load
            this.find(selectors).each(function () {
                setInputValueAttr(this);
            });

            // on keyup
            this.on("keyup", selectors, function () {
                setInputValueAttr(this);
            });
        };

        /**
         * Material Inputs
         */
        $('body').materializeInputs();


    </script>

    <script>
        function checkValue(element) {
            // check if the input has any value (if we've typed into it)
            if ($(element).val())
                $(element).addClass('has-value');
            else
                $(element).removeClass('has-value');
        }

        $(document).ready(function () {
            // Run on page load
            $('.form-control').each(function () {
                checkValue(this);
            })
            // Run on input exit
            $('.form-control').blur(function () {
                checkValue(this);
            });

        });
    </script>

    <script>
        $(document).ready(function () {
            $("#itemSearch").keyup(function () {
                var x = document.getElementById('showSearchDiv');
                if ($(this).val() == "") {
                    x.style.display = 'none';
                } else {
                    x.style.display = 'block';
                }
            });
        });
    </script>
@endsection

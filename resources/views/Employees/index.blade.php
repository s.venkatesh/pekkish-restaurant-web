@extends('layouts.app')
@section('content')

<div class="main-section">
    <div class="max-content-width main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">LIST OF USERS</span>
                <button class="add-btn" data-toggle="modal" data-target="#addemployeesid">+ Add User</button>
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th>Employee Name</th>
                        <th>User Email ID</th>
                        <th>Role</th>
                        <th>Store Name</th>
                        <th>Location</th>
                        <th>Reset Password</th>
                        <th>Actions</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Kim Johnson</td>
                        <td>kim@gmailcom</td>
                        <td>Account Manager</td>
                        <td>All</td>
                        <td>Jamica</td>
                        <td>
                            <a class="reset-password" href="#">Reset</a>
                        </td>
                        <td>
                            <div class="d-flex">
                                <img src="./images/card-edit.svg" alt="">
                                <img class="mx-3" src="./images/delete.svg" alt="">
                            </div>
                        </td>

                    </tr>

                    <tr>
                        <td>Lewis Anderson</td>
                        <td>lewis@gmail.com</td>
                        <td>Store Manager</td>
                        <td>Cava New York</td>
                        <td>Jamica</td>
                        <td>
                            <a class="reset-password" href="#">Reset</a>
                        </td>
                        <td>
                            <div class="d-flex">
                                <img src="./images/card-edit.svg" alt="">
                                <img class="mx-3" src="./images/delete.svg" alt="">
                            </div>
                        </td>

                    </tr>

                    <tr>
                        <td>Gary Pietorson</td>
                        <td>garypietorson@gmail.com</td>
                        <td>Store Employee</td>
                        <td>Cava - Jamaica</td>
                        <td>Jamica</td>
                        <td>
                            <a class="reset-password" href="#">Reset</a>
                        </td>
                        <td>
                            <div class="d-flex">
                                <img src="./images/card-edit.svg" alt="">
                                <img class="mx-3" src="./images/delete.svg" alt="">
                            </div>
                        </td>

                    </tr>

                    <tr>
                        <td>Omari Anderson</td>
                        <td>omari@gmail.com</td>
                        <td>Store Employee</td>
                        <td>Cava - Jamaica</td>
                        <td>Jamica</td>
                        <td>
                            <a class="reset-password" href="#">Reset</a>
                        </td>
                        <td>
                            <div class="d-flex">
                                <img src="./images/card-edit.svg" alt="">
                                <img class="mx-3" src="./images/delete.svg" alt="">
                            </div>
                        </td>

                    </tr>

                    <tr>
                        <td>Kim J</td>
                        <td>jkim@gmailcom</td>
                        <td>Account Manager</td>
                        <td>All</td>
                        <td>Jamica</td>
                        <td>
                            <a class="reset-password" href="#">Reset</a>
                        </td>
                        <td>
                            <div class="d-flex">
                                <img src="./images/card-edit.svg" alt="">
                                <img class="mx-3" src="./images/delete.svg" alt="">
                            </div>
                        </td>

                    </tr>


                    </tbody>
                </table>

            </div>
            <div class="container-fluid p-0 mt-5">
                <div class="d-flex align-items-center justify-content-between mb-4">
                    <span class="heading">ROLES DEFINITION</span>

                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="restaurant-role-card">
                            <div class="d-flex">
                                <img class="role-icon" src="./images/account-manager.svg" alt="">
                            </div>
                            <div>
                                <p class="mt-3 mb-2 d-flex">Account Manager</p>
                                <p class="mb-0 f-12 text-color-grey role-description d-flex">Has complete control over all restaurants including the ability to add new locations and users.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="restaurant-role-card">
                            <div class="d-flex">
                                <img class="role-icon" src="./images/store-manager.svg" alt="">
                            </div>
                            <div>
                                <p class="mt-3 mb-2 d-flex">Store Manager</p>
                                <p class="mb-0 f-12 text-color-grey role-description d-flex">Store Managers will only be able to see the stores assigned to his/her profile.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="restaurant-role-card">
                            <div class="d-flex">
                                <img class="role-icon" src="./images/store-employees.svg" alt="">
                            </div>
                            <div>
                                <p class="mt-3 mb-2 d-flex">Store Employee</p>
                                <p class="mb-0 f-12 text-color-grey role-description d-flex">Will only receive orders from the store assigned to account.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
@endsection

@section('model')

    <div>
        <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog" aria-labelledby="sidebar-option-lable">
            <div class="modal-dialog" role="sidebar options">
                <div class="modal-content bg-black">


                    <div class="modal-body sidebar-options">
                        <header>
                            <img class="logo" src="./images/pekkish-logo.svg" alt="">
                            <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button> -->
                        </header>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate " href="./dashboard.html">
                                        <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                        <span class=" menu-link">Dashboard</span></a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse"
                                       data-target="#orders-menu">
                                        <img class="nav-icons" src="./images/orders.svg" alt="">
                                        <span class="">Orders</span></a>
                                    <div class="collapse" id="orders-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="./orders.html"><span>Orders</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./order_history.html"><span>History</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./menu.html">
                                        <img class="nav-icons" src="./images/menu.svg" alt="">
                                        <span class="menu-link">Menu</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./bussiness_hours.html">
                                        <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                        <span
                                            class=" menu-link">Bussiness Hours</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./accounts.html">
                                        <img class="nav-icons" src="./images/accounts.svg" alt="">
                                        <span
                                            class=" menu-link">Accounts</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./feedback_reviews.html">
                                        <img class="nav-icons" src="./images/feeback.svg" alt="">
                                        <span class=" menu-link">Feedbacks
                                            & Reviews</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./offer_list.html">
                                        <img class="nav-icons" src="./images/offers.svg" alt="">
                                        <span class=" menu-link">Offers</span></a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate active-links" href="#settings-menu" data-toggle="collapse"
                                       data-target="#settings-menu">
                                        <img class="nav-icons" src="./images/settings.svg" alt="">
                                        <span class="">Settings</span></a>
                                    <div class="collapse" id="settings-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1 active-links"
                                                   href="./manage_employees.html"><span>Manage Employees</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./manage_restaurant.html"><span>Manage Restaurants</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./time_settings.html"><span>Time Settings</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <div class="mt-5">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/logout.svg" alt="">
                                            <span class=" menu-link">Logout</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/help.svg" alt="">
                                            <span class=" menu-link">Help</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">

                                            <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                    </li>
                                </div>


                            </ul>

                        </nav>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog" aria-labelledby="user-profile">
            <div class="modal-dialog" role="profile dialog">
                <div class="modal-content bg-black">

                    <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="user-profile">Right Sidebar</h4>
                </div> -->

                    <div class="modal-body">

                        <div class="outerDivFull" >
                            <div class="switchToggle">
                                <input type="checkbox" id="switch">
                                <label for="switch">Toggle</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                            <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                            <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                        </div>
                        <div>
                            <nav class="navbar-options">
                                <ul class="nav flex-column flex-nowrap overflow-hidden">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Update Email Address</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Reset Password</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate " href="#">
                                            <span class=" menu-link">Update <Address></Address></span></a>
                                    </li>
                                </ul>
                        </div>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal add-restaurant right fade " id="addemployeesid" tabindex="-1" role="dialog"
             aria-labelledby="add-restaurant">
            <div class="modal-dialog" id="addnewemployees" role="add employees">
                <div class="modal-content">



                    <div class="modal-body pb-4">
                        <div class="f-20 f-medium">Add Employees</div>



                        <form id="manage-employees-form">
                            <div class="row">
                                <div class="container-fluid" style="margin: 0 0 -28px 0;">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" class="form-control" id="firstname" required>
                                                <label for="firstname">First Name</label>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" class="form-control" id="lastname" required>
                                                <label for="lastname">Last Name</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <div class="form-group input-material">
                                <input type="text" class="form-control" id="emailid" required>
                                <label for="emailid">Email Id</label>
                            </div>
                            <div class="mt-4">
                                <div class="f-14 text-color-grey mb-2">Select Role</div>
                                <div class="flex">
                                    <button class="employee-options option-selected">Admin</button>
                                    <button class="employee-options">Store Manager</button>
                                    <button class="employee-options">Account Manager</button>
                                    <button class="employee-options">Sales Person</button>
                                </div>

                            </div>

                            <div class="mt-4">
                                <div class="f-14 text-color-grey mb-2">Store Name</div>
                                <div class="flex">
                                    <button class="employee-options option-selected">All</button>
                                    <button class="employee-options">Cava - New York</button>
                                    <button class="employee-options">Cava - London</button>

                                </div>

                            </div>
                            <div class="form-group input-material w-50 mr-2">
                                <input type="text" class="form-control" id="joiningdate" required  onfocus="(this.type='date')"
                                       onblur="(this.type='text')">
                                <label for="joiningdate" class="datetype">Joining Date</label>
                            </div>

                            <div class="mt-5 d-flex align-items-center justify-content-between">

                                <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                                <button class="addbtn">ADD EMPLOYEE</button>
                            </div>

                        </form>

                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

    </div>
@endsection

@section('script')
<script>
    $.fn.materializeInputs = function (selectors) {

        // default param with backwards compatibility
        if (typeof (selectors) === 'undefined') selectors = "input, textarea";

        // attribute function
        function setInputValueAttr(element) {
            element.setAttribute('value', element.value);
        }

        // set value attribute at load
        this.find(selectors).each(function () {
            setInputValueAttr(this);
        });

        // on keyup
        this.on("keyup", selectors, function () {
            setInputValueAttr(this);
        });
    };

    /**
     * Material Inputs
     */
    $('body').materializeInputs();


</script>
@endsection

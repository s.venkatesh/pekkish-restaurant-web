@extends('layouts.app')
@section('content')
    <div class="main-section">
        <div class="max-content-width main-section-padding">
            <div class="res-cards">
                <div class="d-flex align-items-center justify-content-between mb-4">
                    <span class="heading">DEFAULT FOOD PREPARATION TIME</span>
                    <button class="add-btn" data-toggle="modal" data-target="#addrestaurantid">+ Add Restaurant</button>
                </div>
                <div class="container-fluid p-0">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                            <div class="manage-restaurant-card">
                                <div class="d-flex f-14 f-medium res-title mb-4">
                                    <img class="resturant-logo-img" src="https://source.unsplash.com/40x40/?hotels"
                                         alt="">
                                    <div>
                                        <p class="mb-0">Erica's Hideaway Restaurant</p>
                                        <p class="mb-0 f-12 text-color-grey">Jamica</p>
                                    </div>

                                </div>
                                <div class="mb-4">
                                    <div class="mb-1 f-14 f-medium">Address</div>
                                    <div class="f-14 ">1235 14th Street, SE, Jamica</div>
                                    <div class="f-14 text-color-grey ">Landmark: Beside Lorem Mall</div>
                                </div>

                                <div>
                                    <div class="mb-1 f-14 f-medium">Bank Account</div>
                                    <div class="f-14 ">100230000456788111</div>
                                    <div class="f-14 text-color-grey ">Bank of Jamica</div>
                                    <div class="f-14 text-color-grey ">Bank Code: ABC0002</div>
                                </div>

                                <div class="d-flex justify-content-end mt-3">
                                    <img class="mx-5" src="./images/card-edit.svg" alt="">
                                    <img src="./images/delete.svg" alt="">
                                </div>

                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                            <div class="manage-restaurant-card">
                                <div class="d-flex f-14 f-medium res-title mb-4">
                                    <img class="resturant-logo-img" src="https://source.unsplash.com/40x40/?restaurant"
                                         alt="">
                                    <div>
                                        <p class="mb-0"> Fireman's Lobster Pit </p>
                                        <p class="mb-0 f-12 text-color-grey">Jamica</p>
                                    </div>

                                </div>
                                <div class="mb-4">
                                    <div class="mb-1 f-14 f-medium">Address</div>
                                    <div class="f-14 ">5465 9th Street, SE, Jamica</div>
                                    <div class="f-14 text-color-grey ">Landmark: Beside Lorem Mall</div>
                                </div>

                                <div>
                                    <div class="mb-1 f-14 f-medium">Bank Account</div>
                                    <div class="f-14 ">100240000456789010</div>
                                    <div class="f-14 text-color-grey ">Bank of Jamica</div>
                                    <div class="f-14 text-color-grey ">Bank Code: ABC0002</div>
                                </div>

                                <div class="d-flex justify-content-end mt-3">
                                    <img class="mx-5" src="./images/card-edit.svg" alt="">
                                    <img src="./images/delete.svg" alt="">
                                </div>

                            </div>
                        </div>


                        <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                            <div class="manage-restaurant-card">
                                <div class="d-flex f-14 f-medium res-title mb-4">
                                    <img class="resturant-logo-img" src="https://source.unsplash.com/40x40/?hotels"
                                         alt="">
                                    <div>
                                        <p class="mb-0"> Potwah Restaurant </p>
                                        <p class="mb-0 f-12 text-color-grey">Jamica</p>
                                    </div>

                                </div>
                                <div class="mb-4">
                                    <div class="mb-1 f-14 f-medium">Address</div>
                                    <div class="f-14 ">2235 14th Street, SE, Jamica</div>
                                    <div class="f-14 text-color-grey ">Landmark: Beside Lorem Mall</div>
                                </div>

                                <div>
                                    <div class="mb-1 f-14 f-medium">Bank Account</div>
                                    <div class="f-14 ">170550000456789010</div>
                                    <div class="f-14 text-color-grey ">Bank of Jamica</div>
                                    <div class="f-14 text-color-grey ">Bank Code: ABC0002</div>
                                </div>

                                <div class="d-flex justify-content-end mt-3">
                                    <img class="mx-5" src="./images/card-edit.svg" alt="">
                                    <img src="./images/delete.svg" alt="">
                                </div>

                            </div>
                        </div>


                        <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                            <div class="manage-restaurant-card">
                                <div class="d-flex f-14 f-medium res-title mb-4">
                                    <img class="resturant-logo-img" src="https://source.unsplash.com/40x40/?beach"
                                         alt="">
                                    <div>
                                        <p class="mb-0"> Murphy's West End </p>
                                        <p class="mb-0 f-12 text-color-grey">Jamica</p>
                                    </div>

                                </div>
                                <div class="mb-4">
                                    <div class="mb-1 f-14 f-medium">Address</div>
                                    <div class="f-14 ">4455 14th Street, SE, Jamica</div>
                                    <div class="f-14 text-color-grey ">Landmark: Beside Lorem Mall</div>
                                </div>

                                <div>
                                    <div class="mb-1 f-14 f-medium">Bank Account</div>
                                    <div class="f-14 ">998545675456789010</div>
                                    <div class="f-14 text-color-grey ">Bank of Jamica</div>
                                    <div class="f-14 text-color-grey ">Bank Code: ABC0002</div>
                                </div>

                                <div class="d-flex justify-content-end mt-3">
                                    <img class="mx-5" src="./images/card-edit.svg" alt="">
                                    <img src="./images/delete.svg" alt="">
                                </div>

                            </div>
                        </div>


                        <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                            <div class="manage-restaurant-card">
                                <div class="d-flex f-14 f-medium res-title mb-4">
                                    <img class="resturant-logo-img" src="https://source.unsplash.com/40x40/?restaurant"
                                         alt="">
                                    <div>
                                        <p class="mb-0"> Best In The West</p>
                                        <p class="mb-0 f-12 text-color-grey">Jamica</p>
                                    </div>

                                </div>
                                <div class="mb-4">
                                    <div class="mb-1 f-14 f-medium">Address</div>
                                    <div class="f-14 ">1135 12th Street, SE, Jamica</div>
                                    <div class="f-14 text-color-grey ">Landmark: Beside Lorem Mall</div>
                                </div>

                                <div>
                                    <div class="mb-1 f-14 f-medium">Bank Account</div>
                                    <div class="f-14 ">897450000456789010</div>
                                    <div class="f-14 text-color-grey ">Bank of Jamica</div>
                                    <div class="f-14 text-color-grey ">Bank Code: ABC0002</div>
                                </div>

                                <div class="d-flex justify-content-end mt-3">
                                    <img class="mx-5" src="./images/card-edit.svg" alt="">
                                    <img src="./images/delete.svg" alt="">
                                </div>

                            </div>
                        </div>


                        <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                            <div class="manage-restaurant-card">
                                <div class="d-flex f-14 f-medium res-title mb-4">
                                    <img class="resturant-logo-img" src="https://source.unsplash.com/40x40/?motel"
                                         alt="">
                                    <div>
                                        <p class="mb-0"> The Blue Mahoe Restaurant </p>
                                        <p class="mb-0 f-12 text-color-grey">Jamica</p>
                                    </div>

                                </div>
                                <div class="mb-4">
                                    <div class="mb-1 f-14 f-medium">Address</div>
                                    <div class="f-14 ">2235 14th Street, SE, Jamica</div>
                                    <div class="f-14 text-color-grey ">Landmark: Beside Lorem Mall</div>
                                </div>

                                <div>
                                    <div class="mb-1 f-14 f-medium">Bank Account</div>
                                    <div class="f-14 ">100230000456789010</div>
                                    <div class="f-14 text-color-grey ">Bank of Jamica</div>
                                    <div class="f-14 text-color-grey ">Bank Code: ABC0002</div>
                                </div>

                                <div class="d-flex justify-content-end mt-3">
                                    <img class="mx-5" src="./images/card-edit.svg" alt="">
                                    <img src="./images/delete.svg" alt="">
                                </div>

                            </div>
                        </div>



                    </div>

                </div>

            </div>

        </div>
    </div>

    <div>
        <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog"
             aria-labelledby="sidebar-option-lable">
            <div class="modal-dialog" role="sidebar options">
                <div class="modal-content bg-black">


                    <div class="modal-body sidebar-options">
                        <header>
                            <img class="logo" src="./images/pekkish-logo.svg" alt="">
                            <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                          aria-hidden="true">&times;</span></button> -->
                        </header>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate active-links" href="{{url('dashboard')}}">
                                        <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                        <span class=" menu-link">Dashboard</span></a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#orders-menu"
                                       data-toggle="collapse"
                                       data-target="#orders-menu">
                                        <img class="nav-icons" src="./images/orders.svg" alt="">
                                        <span class="">Orders</span></a>
                                    <div class="collapse" id="orders-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="{{url('orders')}}"><span>Orders</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="{{url('order_history')}}"><span>History</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{url('menu')}}">
                                        <img class="nav-icons" src="./images/menu.svg" alt="">
                                        <span class="menu-link">Menu</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('business_hours/') }}">
                                        <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                        <span class=" menu-link">Bussiness Hours</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('accounts/') }}">
                                        <img class="nav-icons" src="./images/accounts.svg" alt="">
                                        <span class=" menu-link">Accounts</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('feedback_reviews/') }}">
                                        <img class="nav-icons" src="./images/feeback.svg" alt="">
                                        <span class=" menu-link">Feedbacks
                      & Reviews</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('offer_list/') }}">
                                        <img class="nav-icons" src="./images/offers.svg" alt="">
                                        <span class=" menu-link">Offers</span></a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#settings-menu"
                                       data-toggle="collapse"
                                       data-target="#settings-menu">
                                        <img class="nav-icons" src="./images/settings.svg" alt="">
                                        <span class="">Settings</span></a>
                                    <div class="collapse" id="settings-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1" href="{{ url('manage_employees/')}}"><span>Manage
                            Employees</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="{{ url('manage_restaurant/')}}"><span>Manage
                            Restaurants</span></a>
                                                <a class="nav-link collapsed py-1" href="{{ url('time_settings/')}}"><span>Time Settings</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <div class="mt-5">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/logout.svg" alt="">
                                            <span class=" menu-link">Logout</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/help.svg" alt="">
                                            <span class=" menu-link">Help</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">

                                            <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                    </li>
                                </div>


                                <!-- <li class="nav-item">
                                  <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse" data-target="#orders-menu"><i class="fa fa-table"></i> <span class="">Reports</span></a>
                                  <div class="collapse" id="orders-menu" aria-expanded="false">
                                      <ul class="flex-column pl-2 nav">
                                          <li class="nav-item"><a class="nav-link py-0" href="#"><span>Orders</span></a></li>
                                          <li class="nav-item">
                                              <a class="nav-link collapsed py-1" href="#orders-menusub1" data-toggle="collapse" data-target="#orders-menusub1"><span>Customers</span></a>
                                              <div class="collapse" id="orders-menusub1" aria-expanded="false">
                                                  <ul class="flex-column nav pl-4">
                                                      <li class="nav-item">
                                                          <a class="nav-link p-1" href="#">
                                                              <i class="fa fa-fw fa-clock-o"></i> Daily </a>
                                                      </li>
                                                      <li class="nav-item">
                                                          <a class="nav-link p-1" href="#">
                                                              <i class="fa fa-fw fa-dashboard"></i> Dashboard </a>
                                                      </li>
                                                      <li class="nav-item">
                                                          <a class="nav-link p-1" href="#">
                                                              <i class="fa fa-fw fa-bar-chart"></i> Charts </a>
                                                      </li>
                                                      <li class="nav-item">
                                                          <a class="nav-link p-1" href="#">
                                                              <i class="fa fa-fw fa-compass"></i> Areas </a>
                                                      </li>
                                                  </ul>
                                              </div>
                                          </li>
                                      </ul>
                                  </div>
                              </li> -->
                            </ul>

                        </nav>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog"
             aria-labelledby="user-profile">
            <div class="modal-dialog" role="profile dialog">
                <div class="modal-content bg-black">

                    <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="user-profile">Right Sidebar</h4>
                </div> -->

                    <div class="modal-body">

                        <div class="outerDivFull">
                            <div class="switchToggle">
                                <input type="checkbox" id="switch">
                                <label for="switch">Toggle</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                            <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                            <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                        </div>
                        <div>
                            <nav class="navbar-options">
                                <ul class="nav flex-column flex-nowrap overflow-hidden">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Update Email Address</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Reset Password</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate " href="#">
                                            <span class=" menu-link">Update <Address></Address></span></a>
                                    </li>
                                </ul>
                        </div>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal add-restaurant right fade " id="addrestaurantid" tabindex="-1" role="dialog"
             aria-labelledby="add-restaurant">
            <div class="modal-dialog" id="addnewrestaurant" role="add restaurant">
                <div class="modal-content">



                    <div class="modal-body pb-4">
                        <div class="f-20 f-medium">Add Store</div>



                        <form id="manage-restaurant-form" method="POST" action="{{url('addShop')}}">
                            @csrf
                            <div class="form-group input-material">
                                <input type="text" name="name" class="form-control" id="store-field" required>
                                <label for="store-field">Store Name</label>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="shortname" class="form-control" id="short-name-field" required>
                                <label for="short-name-field">Short Name of Store</label>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="email" class="form-control" id="email-field" required>
                                <label for="email-field">Email</label>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="address" class="form-control" id="address" required>
                                <label for="address">Address</label>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="landmark" class="form-control" id="landmark" required>
                                <label for="landmark">Landmark</label>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="city" class="form-control" id="city" required>
                                <label for="city">City</label>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="bank_acno" class="form-control" id="bankaccountnumber" required>
                                <label for="bankaccountnumber">Bank Account Number</label>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="bank_code" class="form-control" id="bankcode" required>
                                <label for="bankcode">Bank Code</label>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="bank_name" class="form-control" id="bankname" required>
                                <label for="banknames">Bank Name</label>
                            </div>
                            <div class="mt-5 d-flex align-items-center justify-content-between">
                                <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                                <button class="addbtn">ADD STORE</button>
                            </div>
                        </form>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

    </div>

    <script>
        $.fn.materializeInputs = function (selectors) {

            // default param with backwards compatibility
            if (typeof (selectors) === 'undefined') selectors = "input, textarea";

            // attribute function
            function setInputValueAttr(element) {
                element.setAttribute('value', element.value);
            }

            // set value attribute at load
            this.find(selectors).each(function () {
                setInputValueAttr(this);
            });

            // on keyup
            this.on("keyup", selectors, function () {
                setInputValueAttr(this);
            });
        };

        /**
         * Material Inputs
         */
        $('body').materializeInputs();


    </script>
@endsection

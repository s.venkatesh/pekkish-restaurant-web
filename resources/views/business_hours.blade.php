@extends('layouts.app')
@section('content')

<div class="main-section">
    <div class="max-content-width main-section-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 border-right-table ">
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <form class="w-100">
                            <div>
                                <h4 class="heading mb-4">WORKING HOURS</h4>
                                <label class="radio">Always Open
                                    <input type="radio" name="working_status">
                                    <span class="checkround"></span>
                                </label>
                                <label class="radio">Selected Business Hours
                                    <input type="radio" name="working_status">
                                    <span class="checkround"></span>
                                </label>
                                <label class="radio">I will Turn On Manually
                                    <input type="radio" name="working_status">
                                    <span class="checkround"></span>
                                </label>
                            </div>
                            <div>
                                <h4 class="heading mb-4 mt-5">WORKING HOURS</h4>
                                <div class="d-flex align-items-center flex-wrap">
                                    <button class="working-hours-selected">S</button>
                                    <button class="working-hours-selection">M</button>
                                    <button class="working-hours-selection">T</button>
                                    <button class="working-hours-selection">W</button>
                                    <button class="working-hours-selection">T</button>
                                    <button class="working-hours-selection">F</button>
                                    <button class="working-hours-selection">S</button>

                                </div>
                                <div class="container-fluid p-0">
                                    <div class="row working-hours mt-3">
                                        <div class="col-12 col-sm-12 col-md-4">
                                            <p class="f-10 mb-2">For</p>
                                            <select name="all" id="all" class="select-drop-down mb-4">
                                                <option value="all">All</option>

                                            </select>
                                        </div>

                                        <div class="col-12 col-sm-12 col-md-4">
                                            <p class="f-10 mb-2">Form</p>
                                            <select name="from" id="from" class="select-drop-down mb-4">
                                                <option value="7">7 AM</option>
                                                <option value="8">8 AM</option>
                                                <option value="9">9 AM</option>
                                            </select>
                                        </div>

                                        <div class="col-12 col-sm-12 col-md-4">
                                            <p class="f-10 mb-2">To</p>
                                            <select name="to" id="to" class="select-drop-down mb-4">
                                                <option value="10">10 AM</option>
                                                <option value="11">11 AM</option>
                                                <option value="12">12 AM</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button class="add-more-working-hours"> + ADD MORE</button>

                            </div>
                            <div>
                                <label class="check">Mark this for all the days
                                    <input type="checkbox" name="is_name">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </form>

                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 border-left-table">
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <h4 class="heading">DUTY STATUS HISTORY</h4>
                        <form>
                            <select name="days" id="days" class="select-drop-down">
                                <option value="7">last 7 days</option>
                                <option value="14">last 14 days</option>
                                <option value="21">last 21 days</option>
                                <option value="28">last 28 days</option>

                            </select>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-light">
                            <tr>
                                <th>Date</th>
                                <th>Status</th>
                                <th>From - To</th>
                                <th>Duration</th>
                                <th>Reason</th>


                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>01-06-2021</td>
                                <td class="text-color-green">Online</td>
                                <td>7am - 8am</td>
                                <td>1 Hour</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>01-08-2021</td>
                                <td class="text-color-green">Online</td>
                                <td>9am - 9:30am</td>
                                <td>1 Hour</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>01-09-2021</td>
                                <td class="text-color-green">Online</td>
                                <td>7am - 8am</td>
                                <td>1 Hour</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>01-10-2021</td>
                                <td class="text-color-red">Offline</td>
                                <td>7am - 8am</td>
                                <td>1 Hour</td>
                                <td>Auto-off due to max pending orders</td>
                            </tr>
                            <tr>
                                <td>01-10-2021</td>
                                <td class="text-color-green">Online</td>
                                <td>9am - 10am</td>
                                <td>1 Hour</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>01-11-2021</td>
                                <td class="text-color-red">Offline</td>
                                <td>7am - 8am</td>
                                <td>1 Hour</td>
                                <td>Auto-off due to max pending orders</td>
                            </tr>
                            <tr>
                                <td>01-12-2021</td>
                                <td class="text-color-green">Online</td>
                                <td>7am - 8am</td>
                                <td>1 Hour</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>01-13-2021</td>
                                <td class="text-color-red">Offline</td>
                                <td>7am - 8am</td>
                                <td>1 Hour</td>
                                <td>Auto-off due to max pending orders</td>
                            </tr>
                            <tr>
                                <td>01-14-2021</td>
                                <td class="text-color-green">Online</td>
                                <td>7am - 8am</td>
                                <td>1 Hour</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>01-14-2021</td>
                                <td class="text-color-red">Offline</td>
                                <td>1pm - 2px</td>
                                <td>1 Hour</td>
                                <td>Auto-off due to max pending orders</td>
                            </tr>


                            </tbody>
                        </table>
                    </div>


                    <div class="d-flex justify-content-between download f-12 mt-0">
                        <div class="d-flex ">
                            <span>Rows per page:</span>
                            <span>
                                    <div class="form-group rows-per-page">
                                        <select class="form-control" id="selected-rows">
                                            <option>10</option>
                                            <option>20</option>
                                            <option>30</option>
                                            <option>40</option>
                                        </select>
                                    </div>
                                </span>
                            <span class="mx-4">1-10 of 10</span>
                            <span>
                                    <i class="fa fa-angle-left f-20 mx-3" aria-hidden="true"></i>
                                    <i class="fa fa-angle-right f-20" aria-hidden="true"></i>
                                </span>
                        </div>

                        <div>
                            <span class="text-color-green mx-2 f-14 f-medium ">Download Report</span>
                            <img src="./images/download.svg" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('modal')
    <div>
        <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog" aria-labelledby="sidebar-option-lable">
            <div class="modal-dialog" role="sidebar options">
                <div class="modal-content bg-black">


                    <div class="modal-body sidebar-options">
                        <header>
                            <img class="logo" src="./images/pekkish-logo.svg" alt="">
                            <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button> -->
                        </header>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./dashboard.html">
                                        <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                        <span class=" menu-link">Dashboard</span></a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse"
                                       data-target="#orders-menu">
                                        <img class="nav-icons" src="./images/orders.svg" alt="">
                                        <span class="">Orders</span></a>
                                    <div class="collapse" id="orders-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="./orders.html"><span>Orders</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./order_history.html"><span>History</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./menu.html">
                                        <img class="nav-icons" src="./images/menu.svg" alt="">
                                        <span class="menu-link">Menu</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate  active-links" href="./bussiness_hours.html">
                                        <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                        <span
                                            class=" menu-link">Bussiness Hours</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./accounts.html">
                                        <img class="nav-icons" src="./images/accounts.svg" alt="">
                                        <span
                                            class=" menu-link">Accounts</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./feedback_reviews.html">
                                        <img class="nav-icons" src="./images/feeback.svg" alt="">
                                        <span class=" menu-link">Feedbacks
                                            & Reviews</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./offer_list.html">
                                        <img class="nav-icons" src="./images/offers.svg" alt="">
                                        <span class=" menu-link">Offers</span></a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#settings-menu" data-toggle="collapse"
                                       data-target="#settings-menu">
                                        <img class="nav-icons" src="./images/settings.svg" alt="">
                                        <span class="">Settings</span></a>
                                    <div class="collapse" id="settings-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="./manage_employees.html"><span>Manage Employees</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./manage_restaurant.html"><span>Manage Restaurants</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./time_settings.html"><span>Time Settings</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <div class="mt-5">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/logout.svg" alt="">
                                            <span class=" menu-link">Logout</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/help.svg" alt="">
                                            <span class=" menu-link">Help</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">

                                            <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                    </li>
                                </div>


                            </ul>

                        </nav>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog" aria-labelledby="user-profile">
            <div class="modal-dialog" role="profile dialog">
                <div class="modal-content bg-black">

                    <div class="modal-body">

                        <div class="outerDivFull" >
                            <div class="switchToggle">
                                <input type="checkbox" id="switch">
                                <label for="switch">Toggle</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                            <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                            <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                        </div>
                        <div>
                            <nav class="navbar-options">
                                <ul class="nav flex-column flex-nowrap overflow-hidden">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Update Email Address</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Reset Password</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate " href="#">
                                            <span class=" menu-link">Update <Address></Address></span></a>
                                    </li>
                                </ul>
                        </div>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

    </div>
@endsection

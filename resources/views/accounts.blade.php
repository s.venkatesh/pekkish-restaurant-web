@extends('layouts.app')
@section('content')

<div class="main-section">
    <div class="orders-notifications">
        <!-- <div class="bg-black">
          <div class="searching-for-orders max-content-width">
            Searching for new orders
          </div>
        </div> -->


        <!-- <div class="bg-green">
          <div class="received-orders max-content-width">
            <div class="received-orders-content">
              <img src="./images/usericon.svg" alt="">
              <div class="d-flex flex-wrap justify-content-center align-items-center">
                <div class="mx-3 new-order-text">You have new order!</div>

                <div class="mx-3 d-flex"><span>Total 4 Items </span> <span class="mx-1">|</span><span>$</span>
                  <span>2500.25</span>
                </div>
              </div>
            </div>
            <div class="d-flex align-items-center justify-content-center">
              <img class="mr-2" src="./images/alret.svg" alt="">
              <button class="see-details-btn">SEE THE DETAILS</button>
            </div>
          </div>
        </div> -->
    </div>

    <div class="max-content-width main-section-padding">
        <div class="d-flex align-items-center justify-content-between pb-2">
            <h4 class="heading">SALES INSIGHTS</h4>
            <form>
                <select name="days" id="days" class="select-drop-down">
                    <option value="7">last 7 days</option>

                </select>
            </form>
        </div>
        <div class="container-fluid p-0 sales">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-3">
                    <div class="sales-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="sale-type">New Sales</div>
                                <img class="mt-2" src="./images/netsales.svg" alt="">
                            </div>

                            <div class="text-to-end">
                                <div class="sale-percent-up"><span class="mx-2">23.29%</span> <img src="./images/uparrow-green.svg"
                                                                                                   alt=""></div>
                                <div class="sale-value"><span>$</span><span>10,999.32</span></div>
                                <div class="sale-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>
                    <div class="sales-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="sale-type">Total Orders</div>
                                <img class="mt-2" src="./images/totalorders.svg" alt="">
                            </div>

                            <div class="text-to-end">
                                <div class="sale-percent-down"><span class="mx-2">10.23%</span> <img src="./images/down-arrow-red.svg"
                                                                                                     alt=""></div>
                                <div class="sale-value"><span><span>$</span></span>2,345</div>
                                <div class="sale-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>
                    <div class="sales-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="sale-type">New Income</div>
                                <img class="mt-2" src="./images/netincome.svg" alt="">
                            </div>

                            <div class="text-to-end">
                                <div class="sale-percent-up"><span class="mx-2">23.29%</span> <img src="./images/uparrow-green.svg"
                                                                                                   alt=""></div>
                                <div class="sale-value"><span><span>$</span></span>10,999.32</div>
                                <div class="sale-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-9">

                    sale details chat
                </div>
            </div>


            <!--Sales history-->

            <div class="row">
                <div class="col-12 mt-4 mb-2">
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <h4 class="heading">SALES HISTORY</h4>

                    </div>
                </div>
            </div>
            <div class="row itemized-report">

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Total Sales</div>
                        <div class="itemized-report-percent-up">
                <span class="mx-1">
                  <img src="./images/uparrow-green.svg" alt=""></span><span>2.25%</span>
                        </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>3122900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>3900</span></span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Food@Work</div>
                        <div class="itemized-report-percent-up">
                <span>
                  <img src="./images/uparrow-green.svg" alt=""></span><span class="mx-1">2.25%</span>
                        </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>3900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>3900</span></span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Delivery</div>
                        <div class="itemized-report-percent-up"> <span><img src="./images/uparrow-green.svg" alt=""></span>
                            <span class="mx-1">16.50%</span>
                        </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>13900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>1900</span></span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Rejected/Cancelled Orders</div>
                        <div class="itemized-report-percent-down"><span ><img
                                    src="./images/down-arrow-red.svg" alt=""></span>
                            <span class="mx-1">2.25%</span> </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>3900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>3900</span></span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th>Order Id</th>
                        <th>Date</th>
                        <th>Order Type</th>
                        <th>Subtotal</th>
                        <th>GCT</th>
                        <th>Total</th>
                        <th>Commision</th>
                        <th>Discount</th>
                        <th>Net Income</th>
                        <th>Status</th>
                        <th>Payment</th>
                        <th><i class="fa fa-plus" aria-hidden="true"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>#1234-56779</td>
                        <td>03-08-2021</td>
                        <td>Delivery</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>220</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>20</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>215</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>2</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>10</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>5</span></div>
                        </td>
                        <td>Delivery</td>
                        <td>Online</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>#1234-56780</td>
                        <td>03-08-2021</td>
                        <td>Delivery</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>220</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>20</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>215</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>2</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>1</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>15</span></div>
                        </td>
                        <td>Pickup</td>
                        <td>Cash</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>#1234-56781</td>
                        <td>03-08-2021</td>
                        <td>Pickup</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>220</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>10</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>7</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>4</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>10</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>5</span></div>
                        </td>
                        <td>Delivery</td>
                        <td>Online</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>#1234-56782</td>
                        <td>03-08-2021</td>
                        <td>Pickup</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>220</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>20</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>215</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>2</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>10</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>5</span></div>
                        </td>
                        <td>Delivery</td>
                        <td>Online</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>#1234-56783</td>
                        <td>03-08-2021</td>
                        <td>Pickup</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>220</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>20</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>215</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>2</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>10</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>5</span></div>
                        </td>
                        <td>Delivery</td>
                        <td>Online</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>#1234-56784</td>
                        <td>03-08-2021</td>
                        <td>Pickup</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>220</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>20</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>215</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>2</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>10</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>5</span></div>
                        </td>
                        <td>Delivery</td>
                        <td>Online</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>#1234-56784</td>
                        <td>03-08-2021</td>
                        <td>Delivery</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>220</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>20</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>215</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>2</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>10</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>5</span></div>
                        </td>
                        <td>Delivery</td>
                        <td>Online</td>
                        <td></td>
                    </tr>


                    </tbody>
                </table>

            </div>
            <div class="row">

                <div class="col-12">
                    <div class="d-flex justify-content-between download f-12 mt-0">
                        <div class="d-flex ">
                            <span>Rows per page:</span>
                            <span>
                  <div class="form-group rows-per-page">
                    <select class="form-control" id="selected-rows">
                      <option>10</option>
                      <option>20</option>
                      <option>30</option>
                      <option>40</option>
                    </select>
                  </div>
                </span>
                            <span class="mx-4">1-7 of 7</span>
                            <span>
                  <i class="fa fa-angle-left f-20 mx-3" aria-hidden="true"></i>
                  <i class="fa fa-angle-right f-20" aria-hidden="true"></i>
                </span>
                        </div>

                        <div>
                            <span class="text-color-green mx-2 f-14 f-medium ">Download Report</span>
                            <img src="./images/download.svg" alt="">
                        </div>
                    </div>
                </div>

            </div>

            <!--ITEMIZED SALES HISTORY-->

            <div class="row">
                <div class="col-12 mt-4 mb-2">
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <h4 class="heading">ITEMIZED SALES HISTORY</h4>

                    </div>
                </div>
            </div>
            <div class="row itemized-report">
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Total Item</div>
                        <div class="itemized-report-percent-up"> <span ><img
                                    src="./images/uparrow-green.svg" alt=""></span>
                            <span class="mx-1">23.29%</span></div>
                        <div class="itemized-report-value">Chicken Periperi…</div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black"> chicken fry</span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Total Orders</div>
                        <div class="itemized-report-percent-down"> <span ><img
                                    src="./images/down-arrow-red.svg" alt=""></span>
                            <span class="mx-1">2.25%</span></div>
                        <div class="itemized-report-value">223</div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black"> 218</span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Top Item Earning</div>
                        <div class="itemized-report-percent-up"> <span ><img
                                    src="./images/uparrow-green.svg" alt=""></span>
                            <span class="mx-1">2.25%</span></div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>553900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>13900</span></span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Top Section</div>
                        <div class="itemized-report-percent-up"><span ><img
                                    src="./images/uparrow-green.svg" alt=""></span>
                            <span class="mx-1">23.29%</span> </div>
                        <div class="itemized-report-value">Recommended</div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black text-uppercase"> Soups</span>
                        </div>
                    </div>
                </div>



            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th>Order Id</th>
                        <th>Date</th>
                        <th>Order Type</th>
                        <th>Price</th>
                        <th>Payent Mode</th>
                        <th>Payment Status</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Chicken Periperi</td>
                        <td>Recommended</td>
                        <td>223</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>233320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>25000320</span></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Soup 1</td>
                        <td>Soups</td>
                        <td>153</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>3320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>25000320</span></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Meals Combo</td>
                        <td>Combinations</td>
                        <td>223</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>233320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>25000320</span></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Starter 22</td>
                        <td>Starters</td>
                        <td>223</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>233320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>25000320</span></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Burger 1 Ft Long</td>
                        <td>Burger</td>
                        <td>223</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>233320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>25000320</span></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Pizza all Time Fav</td>
                        <td>Pizza</td>
                        <td>223</td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>233320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>320</span></div>
                        </td>
                        <td>
                            <div class="d-flex"><span>$</span> <span>25000320</span></div>
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div>
            <div class="row">

                <div class="col-12">
                    <div class="d-flex justify-content-between download f-12 mt-0">
                        <div class="d-flex ">
                            <span>Rows per page:</span>
                            <span>
                  <div class="form-group rows-per-page">
                    <select class="form-control" id="selected-rows">
                      <option>10</option>
                      <option>20</option>
                      <option>30</option>
                      <option>40</option>
                    </select>
                  </div>
                </span>
                            <span class="mx-4">1-6 of 6</span>
                            <span>
                  <i class="fa fa-angle-left f-20 mx-3" aria-hidden="true"></i>
                  <i class="fa fa-angle-right f-20" aria-hidden="true"></i>
                </span>
                        </div>

                        <div>
                            <span class="text-color-green mx-2 f-14 f-medium ">Download Report</span>
                            <img src="./images/download.svg" alt="">
                        </div>
                    </div>
                </div>

            </div>

            <!--ACCOUNT SETTLEMENT-->

            <div class="row">
                <div class="col-12 mt-4 mb-2">
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <h4 class="heading">ACCOUNT SETTLEMENT</h4>

                    </div>
                </div>
            </div>
            <div class="row itemized-report">


                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Total Disburement</div>
                        <div class="itemized-report-percent-up"> <span ><img
                                    src="./images/uparrow-green.svg" alt=""></span>
                            <span class="mx-1">4.13%</span></div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>433900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>32900</span></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Toral Refund Amount</div>
                        <div class="itemized-report-percent-down"><span ><img
                                    src="./images/down-arrow-red.svg" alt=""></span>
                            <span class="mx-1">3.33%</span> </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>33900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>13900</span></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Total Earnings</div>
                        <div class="itemized-report-percent-up"><span ><img
                                    src="./images/uparrow-green.svg" alt=""></span>
                            <span class="mx-1">8.25%</span> </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>8993900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>883900</span></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Cancelled Orders</div>
                        <div class="itemized-report-percent-up"><span ><img
                                    src="./images/uparrow-green.svg" alt=""></span>
                            <span class="mx-1">1.25%</span> </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>100</span></span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th>Date</th>
                        <th>Transaction Id</th>
                        <th>Total Disbursment</th>
                        <th>Refund Amount</th>
                        <th>Total Earnings</th>
                        <th>Transaction Status</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>01-06-2021</td>
                        <td>ABC000012345677</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>1329.00</span>
                            </div>
                        </td>
                        <td>0</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>1329.00</span>
                            </div>
                        </td>
                        <td class="text-color-green ">Credited</td>

                    </tr>


                    <tr>
                        <td>01-06-2021</td>
                        <td>ABC000012345678</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>1000.00</span>
                            </div>
                        </td>
                        <td>0</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>1000.00</span>
                            </div>
                        </td>
                        <td class="text-color-green ">Credited</td>

                    </tr>

                    <tr>
                        <td>01-06-2021</td>
                        <td>ABC000012345679</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>200.00</span>
                            </div>
                        </td>
                        <td>0</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>500.00</span>
                            </div>
                        </td>
                        <td class="text-color-orange ">Invoiced</td>

                    </tr>

                    <tr>
                        <td>02-08-2021</td>
                        <td>ABC000012345998</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>1200.00</span>
                            </div>
                        </td>
                        <td>0</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>1500.00</span>
                            </div>
                        </td>
                        <td class="text-color-orange ">Invoiced</td>

                    </tr>

                    <tr>
                        <td>02-24-2021</td>
                        <td>ABC000012345999</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>1329.00</span>
                            </div>
                        </td>
                        <td>0</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>1329.00</span>
                            </div>
                        </td>
                        <td class="text-color-green ">Credited</td>

                    </tr>


                    <tr>
                        <td>02-16-2021</td>
                        <td>ABC000012346999</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>1000.00</span>
                            </div>
                        </td>
                        <td>0</td>
                        <td>
                            <div class="d-flex">
                                <span>$</span> <span>1000.00</span>
                            </div>
                        </td>
                        <td class="text-color-green ">Credited</td>

                    </tr>



                    </tbody>
                </table>

            </div>
            <div class="row">

                <div class="col-12">
                    <div class="d-flex justify-content-between download f-12 mt-0">
                        <div class="d-flex ">
                            <span>Rows per page:</span>
                            <span>
                  <div class="form-group rows-per-page">
                    <select class="form-control" id="selected-rows">
                      <option>10</option>
                      <option>20</option>
                      <option>30</option>
                      <option>40</option>
                    </select>
                  </div>
                </span>
                            <span class="mx-4">1-10 of 10</span>
                            <span>
                  <i class="fa fa-angle-left f-20 mx-3" aria-hidden="true"></i>
                  <i class="fa fa-angle-right f-20" aria-hidden="true"></i>
                </span>
                        </div>

                        <div>
                            <span class="text-color-green mx-2 f-14 f-medium ">Download Report</span>
                            <img src="./images/download.svg" alt="">
                        </div>
                    </div>
                </div>

            </div>



        </div>
    </div>
</div>
@endsection

@section('model')
<div>
    <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog"
         aria-labelledby="sidebar-option-lable">
        <div class="modal-dialog" role="sidebar options">
            <div class="modal-content bg-black">


                <div class="modal-body sidebar-options">
                    <header>
                        <img class="logo" src="./images/pekkish-logo.svg" alt="">
                        <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                      aria-hidden="true">&times;</span></button> -->
                    </header>
                    <nav class="navbar-options">
                        <ul class="nav flex-column flex-nowrap overflow-hidden">
                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="./dashboard.html">
                                    <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                    <span class=" menu-link">Dashboard</span></a>
                            </li>

                            <li class="nav-item ">
                                <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse"
                                   data-target="#orders-menu">
                                    <img class="nav-icons" src="./images/orders.svg" alt="">
                                    <span class="">Orders</span></a>
                                <div class="collapse" id="orders-menu" aria-expanded="false">
                                    <ul class="flex-column pl-2 nav">
                                        <li class="nav-item">
                                            <a class="nav-link collapsed py-1" href="./orders.html"><span>Orders</span></a>
                                            <a class="nav-link collapsed py-1" href="./order_history.html"><span>History</span></a>

                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="./menu.html">
                                    <img class="nav-icons" src="./images/menu.svg" alt="">
                                    <span class="menu-link">Menu</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="./bussiness_hours.html">
                                    <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                    <span class=" menu-link">Bussiness Hours</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-truncate active-links" href="./accounts.html">
                                    <img class="nav-icons" src="./images/accounts.svg" alt="">
                                    <span class=" menu-link">Accounts</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="./feedback_reviews.html">
                                    <img class="nav-icons" src="./images/feeback.svg" alt="">
                                    <span class=" menu-link">Feedbacks
                      & Reviews</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="./offer_list.html">
                                    <img class="nav-icons" src="./images/offers.svg" alt="">
                                    <span class=" menu-link">Offers</span></a>
                            </li>


                            <li class="nav-item ">
                                <a class="nav-link collapsed text-truncate" href="#settings-menu" data-toggle="collapse"
                                   data-target="#settings-menu">
                                    <img class="nav-icons" src="./images/settings.svg" alt="">
                                    <span class="">Settings</span></a>
                                <div class="collapse" id="settings-menu" aria-expanded="false">
                                    <ul class="flex-column pl-2 nav">
                                        <li class="nav-item">
                                            <a class="nav-link collapsed py-1" href="./manage_employees.html"><span>Manage
                            Employees</span></a>
                                            <a class="nav-link collapsed py-1" href="./manage_restaurant.html"><span>Manage
                            Restaurants</span></a>
                                            <a class="nav-link collapsed py-1" href="./time_settings.html"><span>Time Settings</span></a>

                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <div class="mt-5">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <img class="nav-icons" src="./images/logout.svg" alt="">
                                        <span class=" menu-link">Logout</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <img class="nav-icons" src="./images/help.svg" alt="">
                                        <span class=" menu-link">Help</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">

                                        <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                </li>
                            </div>


                            <!-- <li class="nav-item">
                              <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse" data-target="#orders-menu"><i class="fa fa-table"></i> <span class="">Reports</span></a>
                              <div class="collapse" id="orders-menu" aria-expanded="false">
                                  <ul class="flex-column pl-2 nav">
                                      <li class="nav-item"><a class="nav-link py-0" href="#"><span>Orders</span></a></li>
                                      <li class="nav-item">
                                          <a class="nav-link collapsed py-1" href="#orders-menusub1" data-toggle="collapse" data-target="#orders-menusub1"><span>Customers</span></a>
                                          <div class="collapse" id="orders-menusub1" aria-expanded="false">
                                              <ul class="flex-column nav pl-4">
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-clock-o"></i> Daily </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-dashboard"></i> Dashboard </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-bar-chart"></i> Charts </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-compass"></i> Areas </a>
                                                  </li>
                                              </ul>
                                          </div>
                                      </li>
                                  </ul>
                              </div>
                          </li> -->
                        </ul>

                    </nav>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->


    <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog"
         aria-labelledby="user-profile">
        <div class="modal-dialog" role="profile dialog">
            <div class="modal-content bg-black">

                <!-- <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                              aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="user-profile">Right Sidebar</h4>
                  </div> -->

                <div class="modal-body">

                    <div class="outerDivFull">
                        <div class="switchToggle">
                            <input type="checkbox" id="switch">
                            <label for="switch">Toggle</label>
                        </div>
                    </div>
                    <div class="text-center">
                        <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                        <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                        <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                    </div>
                    <div>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <span class=" menu-link">Update Email Address</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <span class=" menu-link">Reset Password</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate " href="#">
                                        <span class=" menu-link">Update <Address></Address></span></a>
                                </li>
                            </ul>
                    </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

</div>

@endsection

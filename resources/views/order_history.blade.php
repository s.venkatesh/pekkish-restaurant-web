@extends('layouts.app')
@section('content')

    <div class="main-section">

        <div class="max-content-width main-section-padding">

            <div class="container-fluid p-0 sales">

                <div class="row">
                    <div class="col-12 mt-4 mb-2">
                        <div class="d-flex align-items-center justify-content-between pb-2">
                            <h4 class="heading">ORDERS HISTORY</h4>
                            <!-- <form>
                              <select name="days" id="days" class="select-drop-down">
                                <option value="7">last 7 days</option>

                              </select>
                            </form> -->
                        </div>
                    </div>
                </div>
                <div class="row itemized-report">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                        <div>
                            <div class="itemized-report-title">Top Selling Item</div>
                            <div class="itemized-report-percent-up">
                <span ><img
                        src="./images/uparrow-green.svg" alt=""></span><span class="mx-1">23.29%</span> </div>
                            <div class="itemized-report-value">Chicken Periperi</div>
                            <div class="itemized-report-compare">
                                <span>Compared to previous 7 days</span> -
                                <span class="font-weight-bold text-color-black"> soup 1</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                        <div>
                            <div class="itemized-report-title">Total Orders</div>
                            <div class="itemized-report-percent-down">
                <span ><img
                        src="./images/down-arrow-red.svg" alt=""></span><span class="mx-1">2.25%</span> </div>
                            <div class="itemized-report-value">223</div>
                            <div class="itemized-report-compare">
                                <span>Compared to previous 7 days</span> -
                                <span class="font-weight-bold text-color-black"> 2</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                        <div>
                            <div class="itemized-report-title">Top Item Earning</div>
                            <div class="itemized-report-percent-up">
                <span ><img
                        src="./images/uparrow-green.svg" alt=""></span><span class="mx-1">23.29%</span> </div>
                            <div class="itemized-report-value text-uppercase">$28200.00</div>
                            <div class="itemized-report-compare">
                                <span>Compared to previous 7 days</span> -
                                <span class="font-weight-bold text-color-black text-uppercase"> $28200.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                        <div>
                            <div class="itemized-report-title">Top Section</div>
                            <div class="itemized-report-percent-up">
                <span><img
                        src="./images/uparrow-green.svg" alt=""></span><span class="mx-1">2.25%</span> </div>
                            <div class="itemized-report-value d-flex">Recommended</div>
                            <div class="itemized-report-compare">
                                <span>Compared to previous 7 days</span> -
                                <span class="font-weight-bold text-color-black d-flex">Soups</span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th>Order Id</th>
                            <th>Date</th>
                            <th>Order Type</th>
                            <th>Price</th>
                            <th>Payent Mode</th>
                            <th>Payment Status</th>
                            <th>Order Status</th>
                            <th><i class="fa fa-plus" aria-hidden="true"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>#1234-56779</td>
                            <td>03-08-2021</td>
                            <td>Individual</td>
                            <td class="d-flex"><span>$</span> <span>220.00</span></td>
                            <td>COD</td>
                            <td class="text-color-green">Paid</td>
                            <td class="text-color-green ">Deliverd</td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>#1234-56780</td>
                            <td>03-08-2021</td>
                            <td>Individual</td>
                            <td class="d-flex"><span>$</span> <span>560.00</span></td>
                            <td>COD</td>
                            <td class="text-color-green">Paid</td>
                            <td class="text-color-green ">Deliverd</td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>#1234-56781</td>
                            <td>03-08-2021</td>
                            <td>Individual</td>
                            <td class="d-flex"><span>$</span> <span>220.00</span></td>
                            <td>COD</td>
                            <td class="text-color-green">Paid</td>
                            <td class="text-color-green ">Deliverd</td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>#1234-56782</td>
                            <td>03-09-2021</td>
                            <td>Group Order</td>
                            <td class="d-flex"><span>$</span> <span>220.00</span></td>
                            <td>Creditcard</td>
                            <td class="text-color-red ">Refunded</td>
                            <td class="text-color-red ">Rejected</td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>#1234-56783</td>
                            <td>03-09-2021</td>
                            <td>Food@Work</td>
                            <td class="d-flex"><span>$</span> <span>1520.00</span></td>
                            <td>Creditcard</td>
                            <td class="text-color-green ">Paid</td>
                            <td class="text-color-orange ">Food Preparing</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>#1234-56784</td>
                            <td>03-09-2021</td>
                            <td>Food@Work</td>
                            <td class="d-flex"><span>$</span> <span>3220.00</span></td>
                            <td>UPI</td>
                            <td class="text-color-green ">Paid</td>
                            <td class="text-color-green ">Food Ready</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>#1234-56785</td>
                            <td>03-09-2021</td>
                            <td>Individual</td>
                            <td class="d-flex"><span>$</span> <span>300.00</span></td>
                            <td>UPI</td>
                            <td class="text-color-green ">Paid</td>
                            <td class="text-color-green ">Food Ready</td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>#1234-56786</td>
                            <td>03-09-2021</td>
                            <td>Food@Work</td>
                            <td class="d-flex"><span>$</span> <span>3220.00</span></td>
                            <td>UPI</td>
                            <td class="text-color-green ">Paid</td>
                            <td class="text-color-green ">Food Ready</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>#1234-56787</td>
                            <td>03-09-2021</td>
                            <td>Group Order</td>
                            <td class="d-flex"><span>$</span> <span>1000.00</span></td>
                            <td>Netbanking</td>
                            <td class="text-color-green ">Paid</td>
                            <td class="text-color-orange ">Food Ready</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>#1234-56788</td>
                            <td>03-09-2021</td>
                            <td>Group Order</td>
                            <td class="d-flex"><span>$</span> <span>1000.00</span></td>
                            <td>UPI</td>
                            <td class="text-color-green ">Paid</td>
                            <td class="text-color-orange ">Food Ready</td>
                            <td></td>
                        </tr>


                        </tbody>
                    </table>

                </div>
                <div class="row">

                    <div class="col-12">
                        <div class="d-flex justify-content-between download f-12 mt-0">
                            <div class="d-flex ">
                                <span>Rows per page:</span>
                                <span>
                        <div class="form-group rows-per-page">
                            <select class="form-control" id="selected-rows">
                              <option>10</option>
                              <option>20</option>
                              <option>30</option>
                              <option>40</option>
                            </select>
                          </div>
                    </span>
                                <span class="mx-4">1-10 of 10</span>
                                <span>
                        <i class="fa fa-angle-left f-20 mx-3" aria-hidden="true"></i>
                        <i class="fa fa-angle-right f-20" aria-hidden="true"></i>
                    </span>
                            </div>

                            <div>
                                <span class="text-color-green mx-2 f-14 f-medium ">Download Report</span>
                                <img src="./images/download.svg"
                                     alt="">
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
@section('model')
    <div>
        <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog" aria-labelledby="sidebar-option-lable">
            <div class="modal-dialog" role="sidebar options">
                <div class="modal-content bg-black">


                    <div class="modal-body sidebar-options">
                        <header>
                            <img class="logo" src="./images/pekkish-logo.svg" alt="">
                            <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button> -->
                        </header>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate " href="./dashboard.html">
                                        <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                        <span class=" menu-link">Dashboard</span></a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate active-links" href="#orders-menu" data-toggle="collapse"
                                       data-target="#orders-menu">
                                        <img class="nav-icons" src="./images/orders.svg" alt="">
                                        <span class="">Orders</span></a>
                                    <div class="collapse" id="orders-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1 "
                                                   href="./orders.html"><span>Orders</span></a>
                                                <a class="nav-link collapsed py-1 active-links"
                                                   href="./order_history.html"><span>History</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./menu.html">
                                        <img class="nav-icons" src="./images/menu.svg" alt="">
                                        <span class="menu-link">Menu</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./bussiness_hours.html">
                                        <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                        <span
                                            class=" menu-link">Bussiness Hours</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./accounts.html">
                                        <img class="nav-icons" src="./images/accounts.svg" alt="">
                                        <span
                                            class=" menu-link">Accounts</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./feedback_reviews.html">
                                        <img class="nav-icons" src="./images/feeback.svg" alt="">
                                        <span class=" menu-link">Feedbacks
                                        & Reviews</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./offer_list.html">
                                        <img class="nav-icons" src="./images/offers.svg" alt="">
                                        <span class=" menu-link">Offers</span></a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#settings-menu" data-toggle="collapse"
                                       data-target="#settings-menu">
                                        <img class="nav-icons" src="./images/settings.svg" alt="">
                                        <span class="">Settings</span></a>
                                    <div class="collapse" id="settings-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="./manage_employees.html"><span>Manage Employees</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./manage_restaurant.html"><span>Manage Restaurants</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./time_settings.html"><span>Time Settings</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <div class="mt-5">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/logout.svg" alt="">
                                            <span class=" menu-link">Logout</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/help.svg" alt="">
                                            <span class=" menu-link">Help</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">

                                            <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                    </li>
                                </div>


                            </ul>

                        </nav>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog" aria-labelledby="user-profile">
            <div class="modal-dialog" role="profile dialog">
                <div class="modal-content bg-black">



                    <div class="modal-body">

                        <div class="outerDivFull" >
                            <div class="switchToggle">
                                <input type="checkbox" id="switch">
                                <label for="switch">Toggle</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                            <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                            <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                        </div>
                        <div>
                            <nav class="navbar-options">
                                <ul class="nav flex-column flex-nowrap overflow-hidden">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Update Email Address</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Reset Password</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate " href="#">
                                            <span class=" menu-link">Update <Address></Address></span></a>
                                    </li>
                                </ul>
                        </div>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

    </div>
@endsection


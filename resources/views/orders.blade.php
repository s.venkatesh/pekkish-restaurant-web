@extends('layouts.app')
@section('content')

<div class="main-section">
    <div class="max-content-width main-section-padding">

        <div class="container-fluid p-0 orders">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-4 border-right-table">
                    <div>
                        <div class="align-items-center justify-content-between">
                            <div class="text-uppercase  new-order-title f-16 f-medium">
                                <span>new orders</span>
                                <span>(5)</span>
                                <img class="notify" src="./images/alert-bell.svg" alt="">
                            </div>

                            <div class="new-orders" data-toggle="modal" data-target="#orderdialogg">
                                <div class="d-flex justify-content-between">
                                    <section class="d-flex align-items-center w-100">
                                        <section class="d-flex ">
                                            <img src="./images/personicon.svg" alt="">
                                            <div class="mx-2">
                                                <p class="f-14 d-flex mb-0">You have new order!</p>
                                                <p class="d-flex f-10 mb-0">


                                                    <span class="mx-1">Total</span>
                                                    <span>4</span>
                                                    <span class="mx-1">Items</span>
                                                    <span class="mx-1">|</span>
                                                    <span>$</span>
                                                    <span>2500.25</span>
                                                </p>
                                            </div>
                                        </section>
                                        <span class="individual-more-details">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </span>
                                    </section>

                                </div>
                            </div>

                            <div class=" group-orders">
                                <div class="d-flex justify-content-between">
                                    <section class="d-flex align-items-center w-100">


                                        <section class="d-flex">
                                            <img src="./images/group.svg" alt="">
                                            <div class="mx-2">
                                                <p class="f-14 d-flex mb-0">You have new order!</p>
                                                <p class="d-flex f-10 mb-0">

                                                    <span class="mx-1">Total</span>
                                                    <span>4</span>
                                                    <span class="mx-1">Items</span>
                                                    <span class="mx-1">|</span>
                                                    <span>$</span>
                                                    <span>2500.25</span>
                                                </p>
                                            </div>
                                        </section>
                                        <span class="group-more-details">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </span>
                                    </section>

                                </div>
                            </div>
                            <div class=" workperks-orders">
                                <div class="d-flex justify-content-between">
                                    <section class="d-flex align-items-center w-100">
                                        <section class="d-flex">
                                            <img src="./images/workperks.svg" alt="">
                                            <div class="mx-2">
                                                <p class="f-14 d-flex mb-0">You have new order!</p>
                                                <p class="d-flex f-10 mb-0">
                                                    <span class="mx-1">Total</span>
                                                    <span>4</span>
                                                    <span class="mx-1">Items</span>
                                                    <span class="mx-1">|</span>
                                                    <span>$</span>
                                                    <span>2500.25</span>
                                                </p>
                                            </div>
                                        </section>
                                        <span class="workperks-more-details">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </span>
                                    </section>

                                </div>
                            </div>


                        </div>

                    </div>
                </div>
                <section class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-8 border-left-table">
                    <section class="on-going-orders">
                        <header class="text-uppercase  new-order-title f-16 f-medium d-flex justify-content-between">
                            <div>
                                <span>ONGOING ORDERS</span>
                                <span>(8)</span>
                            </div>
                            <div>
                                <div class="menu-list-items d-flex justify-content-end align-items-center">
                                    <button class="add-section mx-4">
                                        Create New Order
                                    </button>
                                    <form>
                                        <select name="days" id="days" class="select-drop-down">
                                            <option value="7">last 7 days</option>

                                        </select>
                                    </form>

                                </div>
                            </div>
                        </header>
                    </section>
                    <div class="container-fluid">
                        <div class="row res-cards">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="orders-card text-color-grey">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <div class="d-flex align-items-end label-status">
                                                <img src="./images/preparing.png" alt="">
                                                <img src="./images/prepationggroup.svg" alt="">
                                            </div>
                                            <div class="f-14 f-medium mt-2 text-color-black">John’s Group Order</div>
                                            <div class=" f-12">
                                                <img src="./images/time.svg" alt="">
                                                <span>11:45</span><span>am</span> <span>|</span> <span>$</span><span>300.99</span>
                                            </div>

                                        </div>
                                        <div class="text-right">
                                            <div>
                                                <span class="text-color-red f-12">15min</span>
                                                <img src="./images/preparing delay.svg" alt="">
                                            </div>
                                            <div class="f-9 f-italic mt-1 ">
                                                delay in preparing this order
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            <div class="f-12 mt-2">#12345-6789</div>
                                            <div class="f-12">
                                                <span>items</span> <span> - </span> <span>20</span>
                                            </div>
                                        </div>
                                        <div>
                                            <img src="./images/fork.svg" alt="">
                                            <button class="food-ready">FOOD READY <i class="mx-2 fa fa-angle-right"
                                                                                     aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="orders-card text-color-grey">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <div class="d-flex align-items-end label-status">
                                                <img src="./images/preparing.png" alt="">

                                            </div>
                                            <div class="f-14 f-medium mt-2 text-color-black">Simmons Order</div>
                                            <div class=" f-12">
                                                <img src="./images/time.svg" alt="">
                                                <span>11:45</span><span>am</span> <span>|</span> <span>$</span><span>300.99</span>
                                            </div>

                                        </div>
                                        <div class="text-right">
                                            <div>
                                                <span class="text-color-green f-12">10min</span>
                                                <img src="./images/preparing in process.svg" alt="">
                                            </div>
                                            <div class="f-9 f-italic mt-1 ">
                                                Available time for this order
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            <div class="f-12 mt-2">#12345-6789</div>
                                            <div class="f-12">
                                                <span>items</span> <span> - </span> <span>20</span>
                                            </div>
                                        </div>
                                        <div>
                                            <img src="./images/fork.svg" alt="">
                                            <button class="food-ready">FOOD READY <i class="mx-2 fa fa-angle-right"
                                                                                     aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="orders-card text-color-grey">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <div class="d-flex align-items-end label-status">
                                                <img src="./images/foodready.png" alt="">

                                            </div>
                                            <div class="f-14 f-medium mt-2 text-color-black">Tim’s Order</div>
                                            <div class=" f-12">
                                                <img src="./images/time.svg" alt="">
                                                <span>11:45</span><span>am</span> <span>|</span> <span>$</span><span>300.99</span>
                                            </div>

                                        </div>
                                        <div class="text-right">
                                            <div>
                                                <span class="text-color-green f-12">Driver Arrived</span>
                                                <img src="./images/foodpickedup.svg" alt="">
                                            </div>
                                            <div class="f-9 f-italic mt-1 ">
                                                About Robert <img class="driverimg" src="./images/userprofile.svg" alt="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            <div class="f-12 mt-2">#12345-6789</div>
                                            <div class="f-12">
                                                <span>items</span> <span> - </span> <span>20</span>
                                            </div>
                                        </div>
                                        <div>
                                            <img src="./images/fork.svg" alt="">
                                            <button class="food-ready">DISPATCH<i class="mx-2 fa fa-angle-right"
                                                                                  aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="orders-card text-color-grey">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <div class="d-flex align-items-end label-status">
                                                <img src="./images/pickedup.png" alt="">

                                            </div>
                                            <div class="f-14 f-medium mt-2 text-color-black">Katy’s Order</div>
                                            <div class=" f-12">
                                                <img src="./images/time.svg" alt="">
                                                <span>11:45</span><span>am</span> <span>|</span> <span>$</span><span>300.99</span>
                                            </div>

                                        </div>
                                        <div class="text-right">
                                            <div>
                                                <span class="text-color-green f-12">Driver Picked Up</span>
                                                <img src="./images/foodpickedup.svg" alt="">
                                            </div>
                                            <div class="f-9 f-italic mt-1 ">
                                                12:45pm <img class="driverimg" src="./images/time.svg" alt="">
                                            </div>
                                            <div class="f-9 f-italic mt-1 ">
                                                About Robert <img class="driverimg" src="./images/userprofile.svg" alt="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            <div class="f-12 mt-2">#12345-6789</div>
                                            <div class="f-12">
                                                <span>items</span> <span> - </span> <span>20</span>
                                            </div>
                                        </div>
                                        <div>
                                            <img src="./images/fork.svg" alt="">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="orders-card text-color-grey">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <div class="d-flex align-items-end label-status">
                                                <img src="./images/preparing.png" alt="">
                                                <img src="./images/prepationggroup.svg" alt="">
                                            </div>
                                            <div class="f-14 f-medium mt-2 text-color-black">John’s Group Order</div>
                                            <div class=" f-12">
                                                <img src="./images/time.svg" alt="">
                                                <span>11:45</span><span>am</span> <span>|</span> <span>$</span><span>300.99</span>
                                            </div>

                                        </div>
                                        <div class="text-right">
                                            <div>
                                                <span class="text-color-red f-12">15min</span>
                                                <img src="./images/preparing delay.svg" alt="">
                                            </div>
                                            <div class="f-9 f-italic mt-1 ">
                                                delay in preparing this order
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            <div class="f-12 mt-2">#12345-6789</div>
                                            <div class="f-12">
                                                <span>items</span> <span> - </span> <span>20</span>
                                            </div>
                                        </div>
                                        <div>
                                            <img src="./images/fork.svg" alt="">
                                            <button class="food-ready">FOOD READY <i class="mx-2 fa fa-angle-right"
                                                                                     aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="orders-card text-color-grey">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <div class="d-flex align-items-end label-status">
                                                <img src="./images/preparing.png" alt="">

                                            </div>
                                            <div class="f-14 f-medium mt-2 text-color-black">Simmons Order</div>
                                            <div class=" f-12">
                                                <img src="./images/time.svg" alt="">
                                                <span>11:45</span><span>am</span> <span>|</span> <span>$</span><span>300.99</span>
                                            </div>

                                        </div>
                                        <div class="text-right">
                                            <div>
                                                <span class="text-color-green f-12">10min</span>
                                                <img src="./images/preparing in process.svg" alt="">
                                            </div>
                                            <div class="f-9 f-italic mt-1 ">
                                                Available time for this order
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            <div class="f-12 mt-2">#12345-6789</div>
                                            <div class="f-12">
                                                <span>items</span> <span> - </span> <span>20</span>
                                            </div>
                                        </div>
                                        <div>
                                            <img src="./images/fork.svg" alt="">
                                            <button class="food-ready">FOOD READY <i class="mx-2 fa fa-angle-right"
                                                                                     aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="orders-card text-color-grey">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <div class="d-flex align-items-end label-status">
                                                <img src="./images/foodready.png" alt="">

                                            </div>
                                            <div class="f-14 f-medium mt-2 text-color-black">Tim’s Order</div>
                                            <div class=" f-12">
                                                <img src="./images/time.svg" alt="">
                                                <span>11:45</span><span>am</span> <span>|</span> <span>$</span><span>300.99</span>
                                            </div>

                                        </div>
                                        <div class="text-right">
                                            <div>
                                                <span class="text-color-green f-12">Driver Arrived</span>
                                                <img src="./images/foodpickedup.svg" alt="">
                                            </div>
                                            <div class="f-9 f-italic mt-1 ">
                                                About Robert <img class="driverimg" src="./images/userprofile.svg" alt="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            <div class="f-12 mt-2">#12345-6789</div>
                                            <div class="f-12">
                                                <span>items</span> <span> - </span> <span>20</span>
                                            </div>
                                        </div>
                                        <div>
                                            <img src="./images/fork.svg" alt="">
                                            <button class="food-ready">DISPATCH<i class="mx-2 fa fa-angle-right"
                                                                                  aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="orders-card text-color-grey">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <div class="d-flex align-items-end label-status">
                                                <img src="./images/pickedup.png" alt="">

                                            </div>
                                            <div class="f-14 f-medium mt-2 text-color-black">Katy’s Order</div>
                                            <div class=" f-12">
                                                <img src="./images/time.svg" alt="">
                                                <span>11:45</span><span>am</span> <span>|</span> <span>$</span><span>300.99</span>
                                            </div>

                                        </div>
                                        <div class="text-right">
                                            <div>
                                                <span class="text-color-green f-12">Driver Picked Up</span>
                                                <img src="./images/foodpickedup.svg" alt="">
                                            </div>
                                            <div class="f-9 f-italic mt-1 ">
                                                12:45pm <img class="driverimg" src="./images/time.svg" alt="">
                                            </div>
                                            <div class="f-9 f-italic mt-1 ">
                                                About Robert <img class="driverimg" src="./images/userprofile.svg" alt="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            <div class="f-12 mt-2">#12345-6789</div>
                                            <div class="f-12">
                                                <span>items</span> <span> - </span> <span>20</span>
                                            </div>
                                        </div>
                                        <div>
                                            <img src="./images/fork.svg" alt="">

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div>
</div>
@endsection
@section('modal')
    <div>
        <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog" aria-labelledby="sidebar-option-lable">
            <div class="modal-dialog" role="sidebar options">
                <div class="modal-content bg-black">


                    <div class="modal-body sidebar-options">
                        <header>
                            <img class="logo" src="./images/pekkish-logo.svg" alt="">
                            <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button> -->
                        </header>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate " href="./dashboard.html">
                                        <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                        <span class=" menu-link">Dashboard</span></a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate active-links" href="#orders-menu" data-toggle="collapse"
                                       data-target="#orders-menu">
                                        <img class="nav-icons" src="./images/orders.svg" alt="">
                                        <span class="">Orders</span></a>
                                    <div class="collapse" id="orders-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1 active-links"
                                                   href="./orders.html"><span>Orders</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./order_history.html"><span>History</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./menu.html">
                                        <img class="nav-icons" src="./images/menu.svg" alt="">
                                        <span class="menu-link">Menu</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./bussiness_hours.html">
                                        <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                        <span
                                            class=" menu-link">Bussiness Hours</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./accounts.html">
                                        <img class="nav-icons" src="./images/accounts.svg" alt="">
                                        <span
                                            class=" menu-link">Accounts</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./feedback_reviews.html">
                                        <img class="nav-icons" src="./images/feeback.svg" alt="">
                                        <span class=" menu-link">Feedbacks
                                        & Reviews</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./offer_list.html">
                                        <img class="nav-icons" src="./images/offers.svg" alt="">
                                        <span class=" menu-link">Offers</span></a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#settings-menu" data-toggle="collapse"
                                       data-target="#settings-menu">
                                        <img class="nav-icons" src="./images/settings.svg" alt="">
                                        <span class="">Settings</span></a>
                                    <div class="collapse" id="settings-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="./manage_employees.html"><span>Manage Employees</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./manage_restaurant.html"><span>Manage Restaurants</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./time_settings.html"><span>Time Settings</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <div class="mt-5">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/logout.svg" alt="">
                                            <span class=" menu-link">Logout</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/help.svg" alt="">
                                            <span class=" menu-link">Help</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">

                                            <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                    </li>
                                </div>


                                <!-- <li class="nav-item">
                      <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse" data-target="#orders-menu"><i class="fa fa-table"></i> <span class="">Reports</span></a>
                      <div class="collapse" id="orders-menu" aria-expanded="false">
                          <ul class="flex-column pl-2 nav">
                              <li class="nav-item"><a class="nav-link py-0" href="#"><span>Orders</span></a></li>
                              <li class="nav-item">
                                  <a class="nav-link collapsed py-1" href="#orders-menusub1" data-toggle="collapse" data-target="#orders-menusub1"><span>Customers</span></a>
                                  <div class="collapse" id="orders-menusub1" aria-expanded="false">
                                      <ul class="flex-column nav pl-4">
                                          <li class="nav-item">
                                              <a class="nav-link p-1" href="#">
                                                  <i class="fa fa-fw fa-clock-o"></i> Daily </a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link p-1" href="#">
                                                  <i class="fa fa-fw fa-dashboard"></i> Dashboard </a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link p-1" href="#">
                                                  <i class="fa fa-fw fa-bar-chart"></i> Charts </a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link p-1" href="#">
                                                  <i class="fa fa-fw fa-compass"></i> Areas </a>
                                          </li>
                                      </ul>
                                  </div>
                              </li>
                          </ul>
                      </div>
                  </li> -->
                            </ul>

                        </nav>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog" aria-labelledby="user-profile">
            <div class="modal-dialog" role="profile dialog">
                <div class="modal-content bg-black">

                    <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="user-profile">Right Sidebar</h4>
                </div> -->

                    <div class="modal-body">

                        <div class="outerDivFull" >
                            <div class="switchToggle">
                                <input type="checkbox" id="switch">
                                <label for="switch">Toggle</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                            <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                            <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                        </div>
                        <div>
                            <nav class="navbar-options">
                                <ul class="nav flex-column flex-nowrap overflow-hidden">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Update Email Address</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Reset Password</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate " href="#">
                                            <span class=" menu-link">Update <Address></Address></span></a>
                                    </li>
                                </ul>
                        </div>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal orders-list right fade " id="orderdialog" tabindex="-1" role="dialog"
             aria-labelledby="orders-list">
            <div class="modal-dialog" id="addnewrestaurant" role="orders list">
                <div class="modal-content">



                    <div class="tabbable boxed ">
                        <ul class="parentTabs nav nav-tabs">
                            <li class="active pickedup"><a href="#PICKUP">PICKUP</a>
                            </li>
                            <li class="delivery"><a href="#DELIVERY">DELIVERY</a>
                            </li>
                            <li class="foodwork"><a href="#FOODWORK">FOODWORK</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="PICKUP">
                                <div class="tabbable">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#sub11">Tab 1.1</a>
                                        </li>
                                        <li><a href="#sub12">Tab 1.2</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="sub11">
                                            <p>Tab 1.1</p>
                                        </div>
                                        <div class="tab-pane fade" id="sub12">
                                            <p>Tab 1.2</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="DELIVERY">
                                <div class="tabbable">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#sub21">Tab 2.1</a>
                                        </li>
                                        <li><a href="#sub22">Tab 2.2</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="sub21">
                                            <p>Tab 2.1</p>
                                        </div>
                                        <div class="tab-pane fade" id="sub22">
                                            <p>Tab 2.2</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="FOODWORK">
                                <div class="tabbable">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#sub31">Tab 3.1</a>
                                        </li>
                                        <li><a href="#sub32">Tab 3.2</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="sub31">
                                            <p>Tab 3.1</p>
                                        </div>
                                        <div class="tab-pane fade" id="sub32">
                                            <p>Tab 3.2</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

    </div>
@endsection

@section('script')
<script>
    $("ul.nav-tabs a").click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    </script>
@endsection

<?php

use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\CouponController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', 'HomeController@home');
Route::get('/', function () {
    return view('welcome');
});

Route::post('/signin', 'AuthController@signin');
Route::post('/signup', 'AuthController@signup');

//if(session()->has("user")) {
//Route::group(array('middleware'=>['EnsureTokenIsValid']),function() {
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/menu', 'MenuController@index');
    Route::post('/addItem', 'MenuController@store');
    Route::get('/orders', 'OrderController@index');
    Route::get('/order_history', 'OrderController@history');
    Route::get('/accounts', 'AcountsController@index');
    Route::get('/business_hours', 'BusinesshoursController@index');
    Route::get('/feedback_reviews', 'FeedbackReviewController@index');
    Route::get('/offer_list', 'OfferController@index');
    Route::get('/manage_employees', 'EmployeeController@index');
    Route::get('/time_settings', 'SettingController@index');
    Route::get('/manage_restaurant', 'RestaurantController@index');
    Route::post('/addShop', 'RestaurantController@store');


    Route::post('/contact-us', 'ContactUsController@ContactUs');

    Route::post('/my-orders', 'OrderController@index');


    Route::resource('coupons', CouponController::class)->except([
        'create', 'edit', 'show'
    ]);
//}








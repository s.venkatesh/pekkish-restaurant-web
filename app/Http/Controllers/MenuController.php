<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Menu\MenuInterface as MenuInterface;
use Illuminate\Support\Facades\Validator;
class MenuController extends Controller
{
    public $menu;

    public function __construct(MenuInterface $menu)
    {
        $this->menu = $menu;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menulist = $this->menu->menulist();
        $cuisines = $this->menu->cuisines();
        $cuisines = $cuisines['data'];
//dd($cuisines);
        return view('menu',compact('cuisines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $data=[];
        $products=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'description' => ['required', 'string'],
                'unit_price' => ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back();
        }

        $data['name'] = $request->name;
        $data['description'] = $request->description;
        $data['photos'] = $request->photos;
        $data['unit_price'] = $request->unit_price;
        $data['service_line'] = $request->service_line;
        $data['available_days'] = $request->available_days;
        $data['menu_items_desc'] = $request->menu_items_desc;
        $data['set_max_order_number'] = $request->set_max_order_number;
        $data['cuisine_id'] = $request->cuisine_id;
        $data['available_ststus'] = $request->available_ststus;
        $data['Modifier'] = $request->Modifier;
        $data['Modifier_name'] = $request->Modifier_name;
        $data['optional'] = $request->optional;
        $data['selection'] = $request->selection;
        $data['variant'] = $request->variant;
        $data['price'] = $request->price;
        $data['added_by'] = $request->added_by;
        $data['status'] = $request->status ?? 1;
        $data['variants'] = $request->variants;

        try {
            $additem = $this->menu->addItem($data);
            // dd($result);
            return view('menu');
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Repositories\Auth\AuthInterface as AuthInterface;
use Illuminate\Support\Facades\Validator;
class AuthController extends Controller
{
    public $auth;

    public function __construct(AuthInterface $auth)
    {
        $this->auth = $auth;
    }

    public function signin(Request $request){

        $data=[];
        $validator = Validator::make($request->all(),
            [
                'email'    => ['required', 'string', 'email'],
                'password' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back();
        }

        $data['email'] = $request->email;
        $data['password'] = $request->password;
        $data['fcm_id'] = "web";
        $data['request_type'] = "web";
        $data['device_type'] = "web";
        $data['role'] = "seller";
        try {
        $result = $this->auth->signIn($data);
       // dd($result);
            session(['token' => $result["access_token"],'user_details'=>$result["user"]]);
            return view('dashboard');
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Signin Failed'], 200);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function signup(Request $request)
    {

        $data=[];
        $validator = Validator::make($request->all(),
            [
                'store_name' => ['required', 'string'],
                'store_address' => ['required', 'string'],
                'firstname' => ['required', 'string'],
                'lastname' => ['required'],
                'email' => ['required'],
                'phone' => ['required']
            ]
        );

        if ($validator->fails()) {
            /*$errors = [];
            foreach(json_decode($validator->messages()) as $key => $value)
                $errors[] = $value[0];
            return response()->json([ 'Status' => "failed", "Response" => $errors ], 400);*/
            return redirect()->back();
        }

        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['store_name'] = $request->store_name;
        $data['store_address'] = $request->store_address;
        $data['firstname'] = $request->firstname;
        $data['lastname'] = $request->lastname;
        $data['role'] = "seller";
        try {
            $result = $this->auth->signUp($data);
            //return view('/');
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Signup Failed'], 200);
        }
    }
}

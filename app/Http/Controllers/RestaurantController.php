<?php

namespace App\Http\Controllers;
use App\Repositories\Shop\ShopInterface as ShopInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class RestaurantController extends Controller
{
    public $shop;

    public function __construct(ShopInterface $shop)
    {
        $this->shop = $shop;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('restaurants.index');
        $user = session()->get('user_details');
        $id =$user['id'];
        $shops = $this->shop->shops($id);
        $shops = $shops['data'];
        return view('restaurants.index',compact('shops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'email' => ['required', 'string'],
                'address' => ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back();
        }
        $data['name'] = $request->name;
        $data['meta_title'] = $request->shortname;
        $data['email'] = $request->email;
        $data['address'] = $request->address;
        $data['landmark'] = $request->landmark;
        $data['city'] = $request->city;
        $data['bank_acno'] = $request->bank_acno;
        $data['bank_code'] = $request->bank_code;
        $data['bank_name'] = $request->bank_name;

        try {
            $result = $this->shop->addShop($data);
            // dd($result);
            return view('restaurants.index');
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

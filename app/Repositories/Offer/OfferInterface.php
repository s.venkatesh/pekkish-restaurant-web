<?php
/**
 * Created by PhpStorm.
 * User: Venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Offer;


interface OfferInterface
{
    public function offers();
    public function addOffer();
}

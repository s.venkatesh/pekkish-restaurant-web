<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Offer;
use App\Repositories\Offer\OfferInterface as OfferInterface;
use Illuminate\Support\Facades\Http;

class OfferRepository implements OfferInterface
{
    public function offers()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('OFFERS'));

        $result = $response->json();
        dd($result);
        if($result["message"] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function addOffer()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ADDOFFER'));

        $result = $response->json();
        dd($result);
        if($result['success']) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

}

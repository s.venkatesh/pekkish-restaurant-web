<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Account;
use App\Repositories\Account\AccountInterface as AccountInterface;
use Illuminate\Support\Facades\Http;

class AccountRepository implements AccountInterface
{
    public function accounts($data)
    {
        //dd($data);
        $response = Http::get(getUrl('ACCOUNTS'),$data);
        $result = $response->json();
        //dd($result);
        if($result["message"] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
 
}

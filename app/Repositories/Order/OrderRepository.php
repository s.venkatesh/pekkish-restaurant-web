<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Order;
use App\Repositories\Order\OrderInterface as OrderInterface;
use Illuminate\Support\Facades\Http;

class OrderRepository implements OrderInterface
{
    public function orders($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('ORDERS'),$id);

        $result = $response->json();
        dd($result);
        if($result["message"] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function orderHistory()
    {
        //dd(getUrl('ORDERHISTORY'));
        //dd(session()->get('token'));
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get('https://pekkish.glovision.co/pekkish-dev/api/v1/order/history');

        /*$response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('ORDERHISTORY'));*/

        $result = $response->json();
        dd($result);
        if($result['success']) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

}

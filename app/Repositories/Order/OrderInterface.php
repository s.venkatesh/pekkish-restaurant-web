<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Order;


interface OrderInterface
{
    public function orders($id);
    public function orderHistory();
}

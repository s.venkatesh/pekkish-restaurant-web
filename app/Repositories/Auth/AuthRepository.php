<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Auth;
use App\Repositories\Auth\AuthInterface as AuthInterface;
use Illuminate\Support\Facades\Http;

class AuthRepository implements AuthInterface
{
    public function signIn($data)
    {
        //dd($data);
//        $response = Http::POST('http://localhost/g/pekkish/pekkish/api/v1/auth/login',[
//          "email" => "seller@glovision.co",
//          "password" => "123456",
//          "fcm_id" => "web",
//          "request_type" => "web",
//          "device_type" => "web",
//          "role" => "seller",
//        ]);

        $response = Http::POST(getUrl('SIGNIN'),$data);
        $result = $response->json();
        //dd($result);
        if($result["message"] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function signUp($data)
    {
        //dd($data);
        $response = Http::POST(getUrl('SIGNUP'),$data);
        $result=$response->json();
        if($result["message"] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

}

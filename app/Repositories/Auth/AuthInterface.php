<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Auth;


interface AuthInterface
{
    public function signIn($data);
    public function signUp($data);

}

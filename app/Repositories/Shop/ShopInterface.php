<?php
/**
 * Created by PhpStorm.
 * User: Venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Shop;


interface ShopInterface
{
    public function shops($id);
    public function addShop();
}

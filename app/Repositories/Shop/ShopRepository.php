<?php
/**
 * Created by PhpStorm.
 * User: vyadati
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Shop;
use App\Repositories\Shop\ShopInterface as ShopInterface;
use Illuminate\Support\Facades\Http;

class ShopRepository implements ShopInterface
{
    public function shops($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('SHOPS'),'7');

        $result = $response->json();
        dd($result);
        if($result["message"] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function addShop()
    {

        /*$response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get('https://pekkish.glovision.co/pekkish-dev/api/shops/insert/');*/

        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ADDSHOP'));

        $result = $response->json();
        dd($result);
        if($result['success']) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

}

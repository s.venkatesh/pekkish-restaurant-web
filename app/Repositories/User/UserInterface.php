<?php
/**
 * Created by PhpStorm.
 * User: Venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\User;


interface UserInterface
{
    public function users();
    public function addUser();
}

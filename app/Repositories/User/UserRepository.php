<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\User;
use App\Repositories\User\UserInterface as UserInterface;
use Illuminate\Support\Facades\Http;

class UserRepository implements UserInterface
{
    public function users()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('USERS'));

        $result = $response->json();
        dd($result);
        if($result["message"] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function addUser()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ADDUSER'));

        $result = $response->json();
        dd($result);
        if($result['success']) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

}

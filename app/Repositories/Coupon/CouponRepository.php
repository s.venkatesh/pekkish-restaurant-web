<?php
/**
 * Created by PhpStorm.
 * User: vyadati
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Coupon;
use App\Repositories\Coupon\CouponInterface as CouponInterface;
use Illuminate\Support\Facades\Http;

class CouponRepository implements CouponInterface
{

    function __construct() {

    }

    public function getAll()
    {
        $data=[];
        $offer["status"]="active";
        $offer["name"]="SALARYDAY";
        $offer["line-one"]="New year promotion";
        $offer["line-two"]="Save 10% OFF on wines";
        $offer["details"]="100 of 250 Coupon Users";
        array_push($data,$offer);
        $offer["status"]="active";
        $offer["name"]="PARTYMODE";
        $offer["line-one"]="New year promotion";
        $offer["line-two"]="Save 10% OFF on wines";
        $offer["details"]="100 of 250 Coupon Users";
        array_push($data,$offer);
        $offer["status"]="expired";
        $offer["name"]="PARTYMODE";
        $offer["line-one"]="New year promotion";
        $offer["line-two"]="Save 10% OFF on wines";
        $offer["details"]="100 of 250 Coupon Users";
        array_push($data,$offer);
        $offer["status"]="active";
        $offer["name"]="SALARYDAY";
        $offer["line-one"]="New year promotion";
        $offer["line-two"]="Save 10% OFF on wines";
        $offer["details"]="100 of 250 Coupon Users";
        array_push($data,$offer);

        $response = Http::get(config('ALL_COUPONS'));
//        dd($response->json()["data"]);

        return $data;
    }

    public function find($id)
    {
        return [];
    }

    public function delete($id)
    {
        return [];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Menu;


interface MenuInterface
{

   public function menulist();
   public function cuisines();
   public function addItem($data);

}

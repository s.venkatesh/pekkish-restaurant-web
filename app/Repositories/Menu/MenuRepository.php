<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Menu;
use App\Repositories\Menu\MenuInterface as MenuInterface;
use Illuminate\Support\Facades\Http;

class MenuRepository implements MenuInterface
{
    public function menulist()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('MENULIST'));
        $result = $response->json();
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function cuisines()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('CUISINES'));
        $result = $response->json();
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function addItem($data)
    {
        $item = json_encode(array("products"=>$data));
dd($item);
         $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ADDITEM'),$item);
        $result = $response->json();
        dd($result);
        if($result["message"] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }


}
